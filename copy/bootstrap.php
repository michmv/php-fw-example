<?php

use Symfony\Component\Dotenv\Dotenv;
use App\Example;

require_once(__DIR__.'/vendor/autoload.php');

(new Dotenv())->loadEnv(__DIR__.'/config/.env');
$config = include(__DIR__.'/config/config.php');

date_default_timezone_set($config['locale']['timezone']);

echo Example::main($config);
