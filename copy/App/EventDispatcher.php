<?php

namespace App;

use MMV\FW\Example\EventDispatcherAbstract;
use MMV\FW\Example\Routes;
use MMV\FW\Example\Backend\MenuItemLeft;

class EventDispatcher extends EventDispatcherAbstract
{
    /**
     * You can set custom routes here
     */
    public function setRoutes(Routes $routes): Routes
    {
        $applyRoutesForThisApplication = new class() {
            public function execute($routes, $path) {
                require($path);
                return $routes;
            }
        };
        $path = $this->app->config['__DIR__'] . '/config/routes.php';
        $routes = $applyRoutesForThisApplication->execute($routes, $path);

        return $routes;
    }


    /**
     * Type: [ 'first' => [ MMV\FW\Example\Backend\MenuItemLeft ]
     *       , 'last'  => [ MMV\FW\Example\Backend\MenuItemLeft ]
     *       ]
     *
     * @param array $list
     * @return array
     */
    public function buildBackendLeftMenu(array $list): array
    {
        $exampleMenu = [
            MenuItemLeft::item('Example 1', 'home'),
            MenuItemLeft::item('Example 4', 'home'),
            MenuItemLeft::separator(),
            MenuItemLeft::list('Example 2', '', [
                MenuItemLeft::item('Example 3', 'home'),
            ]),
            MenuItemLeft::item('Example 5', 'home'),
            MenuItemLeft::list('Example 7', '', [
                MenuItemLeft::item('Example 8', 'home'),
            ]),
        ];

        $list['first'] = array_merge($list['first'], $exampleMenu);

        return $list;
    }
}
