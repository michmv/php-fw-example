<?php

namespace App;

use MMV\FW\Core\ApplicationInterface;
use MMV\FW\Core\ApplicationTrait;
use MMV\FW\Core\RoutesInterface;
use MMV\FW\Example\Services;
use Throwable;
use MMV\FW\Example\PhpErrorHandler;
use MMV\FW\Example\Response;

class Example implements ApplicationInterface
{
    use ApplicationTrait;
    use Services\EventDispatcherTrait;
    use Services\RequestTrait;
    use Services\RoutesTrait;
    use Services\LoggerTrait;
    use Services\TemplateTrait;
    use Services\EmailTrait;
    use Services\CacheTrait;
    use Services\SecurityTrait;
    use Services\ResponseTrait;
    use Services\LocaleTrait;
    use Services\DbTrait;
    use Services\SessionTrait;
    use Services\HelperTrait;
    use Services\ValidatorTrait;
    use Services\AuthTrait;

    public $config = [];

    public function __construct(array &$config)
    {
        $this->config = $config;
    }

    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     * 
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters)
    {
        // check max post size
        if(!\MMV\FW\Example\Middleware\PostSizeValid::handler($this->request()))
            return $this->response()->abort(413);

        // trim input parameters
        \MMV\FW\Example\Middleware\TrimParameters::handler($this->request());

        // csrf protection
        if(!\MMV\FW\Example\Middleware\VerifyCsrfToken::handler($this->request(),
            $this->session(), $this->config['security']['csrf']['name']))
                return $this->response()->abort(419);

        return null;
    }

    /**
     * @param mixed $response
     * @return mixed
     */
    public function terminate($response)
    {
        // session
        $this->session()->save();

        if(!$response instanceof Response) {
            // Headers will be send manually
            $this->response()->response->sendHeaders();
        }

        return $response;
    }

    public static function getRoutes($app): RoutesInterface
    {
        /** @var self $app */
        return $app->routes();
    }

    /**
     * @param Throwable $exception
     * @param array $config
     * @param object|null $app
     * @return mixed
     */
    public static function phpErrorHandler(Throwable $exception, array $config, ?object $app=null): string
    {
        return PhpErrorHandler::handler($exception, $config, $app);
    }
}
