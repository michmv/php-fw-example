<?php

return [

    'dashboard' => 'Приборка',

    'menu' => [
        'change-password' => 'Изменить пароль',
        'signout' => 'Выйти',
        'signout-all' => 'Выйти из всех устройств',
        'open-site' => 'Открыть сайт',
        'management' => 'Администрирование',
    ],

    'role' => [
        'title' => 'Роли пользователей',
        'default' => 'Роль назначаемая новым пользователям по умолчанию',
        'columnName' => 'Имя',
        'columnDescription' => 'Описание',
        'role' => 'Роль',
        'resources' => 'Ресурсы',
    ],

    'user' => [
        'title' => 'Пользователи',
        'columnRole' => 'Роль',
        'columnLogin' => 'Логин',
        'actionDelete' => 'Удалить',
        'actionUpdate' => 'Редактировать',
        'new_user' => 'Создать пользователя',
        'update_user' => 'Обновить пользователя',
        'question' => 'Удалить пользователя?',
        'warning' => 'Удаление пользователя может нарушить целостность данных в базе, удалите пользователя только в том случае если в этом абсолютно уверены. Лучший вариант не удалять пользователя а сменить ему роль.',
    ],

    'messages' => [
        'userCreated' => 'Создана новая запись.',
        'userUpdated' => 'Запись обновлена.',
    ],

    'form' => [
        'update' => 'Изменить',
        'create' => 'Создать',
        'cancel' => 'Отмена',
        'delete' => 'Удалить',
    ],
];
