<?php

return [

    'locale' => [

        'default' => 'ru',

        'timezone' => 'Europe/Moscow',

        /**
         * Path relative to __DIR__
         * Must be subdirectory with locale
         * 
         * Example, dir translations:
         *   ru
         *     messages.php
         *   en
         *     messages.php
         * 
         * In order to use the ICU Message Format, the message domain has to 
         * be suffixed with `+intl-icu` (example: messages+intl-icu.php)
         * online - http://format-message.github.io/icu-message-format-for-translators/
         * docs - https://unicode-org.github.io/icu/userguide/format_parse/messages/
         */ 
        'directories' => [
            '/translations',
        ],
    ],

    'debug' => (bool)(($_ENV['DEBUG'] ?? 'false') === 'true'),

    /**
     * Root directory for application
     */
    '__DIR__' => __DIR__.'/..',

    'logger' => [

        /**
         * Path relative to __DIR__
         */
        'path' => '/storage/logs',

        'phpDebugBar' => (bool)(($_ENV['LOGGER_PHP_DEBUG_BAR'] ?? 'false') === 'true'),

        'jsConsoleLog' => (bool)(($_ENV['LOGGER_JS_CONSOLE_LOG'] ?? 'false') === 'true'),

        'httpHeaderLog' => (bool)(($_ENV['LOGGER_HTTP_HEADER_LOG'] ?? 'false') === 'true'),

        'firePhp' => (bool)(($_ENV['LOGGER_FIRE_PHP'] ?? 'false') === 'true'),
    ],

    'cache' => [

        'timeDefault' => 300,

        /**
         * Path relative to __DIR__
         */
        'path' => '/storage/cache',
    ],

    /**
     * Config for template engine
     */
    'template' => [

        /**
         * Path to directory with templates, path relative to __DIR__
         */
        'directories' => [
            'fw-example' => '/vendor/mmv/fw-example/resources',
            '__main__'   => '/templates',
        ],

        /**
         * Path to directory cache, path relative to __DIR__
         */
        'cache' => '/storage/templates',

        'logger' => (bool)(($_ENV['LOGGER_TEMPLATE'] ?? 'false') === 'true'),
    ],

    
    /**
     * Global settings for head block of site
     */
    'head' => [

        /**
         * Globals sources
         */
        'sources' => [
            'example' => '<!-- example attache global resource -->',
        ],

        'SEO' => [
            'title'       => 'siteTitle',
            'keywords'    => '',
            'description' => '',
        ],
    ],

    
    'security' => [
        /**
         * This key is used by the encrypter service and should be set
         * to a random, 32 character string, otherwise these encrypted strings
         * will not be safe. Please do this before deploying an application!
         */
        'key' => $_ENV['APP_KEY'],

        'cipher' => 'AES-256-CBC',

        'bcrypt' => [
            'rounds' => 10,
        ],

        /**
         * Name var
         */
        'csrf' => [

            'name' => '__token',

            'timeLife' => 7200, // 60 * 60 * 2
        ],
    ],

    /**
     * Config for connect database
     */
    'database' => [

        'connect' => [
            'driver' => 'mysql',
            'host' => $_ENV['DB_HOST'] ?? '127.0.0.1',
            'port' => $_ENV['DB_PORT'] ?? '3306',
            'database' => $_ENV['DB_DATABASE'] ?? 'forge',
            'username' => $_ENV['DB_USERNAME'] ?? 'forge',
            'password' => $_ENV['DB_PASSWORD'] ?? '',
            'unix_socket' => $_ENV['DB_SOCKET'] ?? '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => $_ENV['DB_PREFIX_TABLE'] ?? '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => $_ENV['MYSQL_ATTR_SSL_CA'] ?? '',
            ]) : [],
        ],

        'logger' => (bool)(($_ENV['LOGGER_DATABASE'] ?? 'false') === 'true'),

        'migration' => [

            /**
             * Path relative to __DIR__
             */
            'pathMigrations' => '/migrations',

            /**
             * Name table
             */
            'tableMigrates' => 'migrations',
        ]
    ],

    'session' => [

        /**
         * Name var in cookie for session id
         */
        'nameCookieVar' => 'uid',

        /**
         * Name table for sessions
         */
        'nameTable' => 'sessions',

        /**
         * Seconds
         */
        'duration' => 14400, // 60 * 60 * 4
    ],

    /**
     * Options for write cookie
     */
    'cookie' => [

        'path' => $_ENV['DOMAIN_PATH'] ?? '/',
        'domain' => $_ENV['DOMAIN_HOST'] ?? null,
        'secure' => null,
    ],

    'email' => [

        'transport' => $_ENV['EMAIL_TRANSPORT'] ?? 'log',

        'log' => [

            /**
             * Path relative to __DIR__
             */
            'path' => '/storage/emails',
        ],

        'from' => [
            'address' => 'main@admin.ru',
            'name' => 'Admin',
        ],
    ],

    'auth' => [

        /**
         * Turn registration function for new user
         */
        'registration' => true,

        /**
         * Authorization only for users who have confirmed their email address
         */
        'signinOnlyAfterConfirmedEmail' => true,

        /**
         * Default role ID for the user after registration
         */
        'defaultRoleId' => 2,

        /**
         * Check access fot resource after singin for redirect
         */
        'accessToBackend' => 'backend',

        /**
         * Name route
         */
        'nameRoute' => [
            'home' => 'home',
            'backend' => 'dashboard'
        ],

        /**
         * Path relative to __DIR__
         */
        'pathToAccess' => '/config/access.php',
    ],
];
