<?php

use MMV\Auth\Low\Access\Resource;
use MMV\Auth\Low\Access\Role;

/**
 * Resources
 */
$resources = [
        new Resource('backend',              'access@resources.backend'),
        new Resource('administration.users', 'access@resources.administration_users', 'access@groups.developer'),
    ];

/**
 * Role
 */
$administration = new Role(1, 'administration', ['backend', 'administration.users']);
$administration->title = 'access@title.administration';
$administration->description = 'access@description.administration';

/**
 * Role
 */
$user = new Role(2, 'user', []);
$user->title = 'access@title.user';
$user->description = 'access@description.user';

/**
 * Return
 */
return [

    'roles' => [
        $administration,
        $user,
    ],

    'resources' => $resources,
];
