<?php

use App\Example;

/** @var \MMV\FW\Example\Routes $routes */

// See https://symfony.com/doc/current/routing.html about routing
$routes->add('home', '/')
    ->callback(function(Example $app) {
        // See https://twig.symfony.com/ about template engine

        return $app->response()->view('homepage.html.twig', ['message'=>'Hello World!']);
    });

// Dashboard page
$routes->add('dashboard', '/backend')
    ->controller(MMV\FW\Example\Controllers\Dashboard::class, 'dashboard');

// Authentication
MMV\FW\Example\Controllers\Auth::routes($routes, 'user');

// Backend
MMV\FW\Example\Controllers\User::routes($routes, 'backend');
