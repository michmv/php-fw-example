<?php

use MMV\FW\Example\Db\Migration;
use Illuminate\Database\Schema\Blueprint;

class AuthModule extends Migration
{
    public $prefixModule = 'auth_';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema()->create($this->prefixModule.'users', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email', 64)->unique();
            $table->string('password', 80);
            $table->string('login')->unique();
            $table->boolean('email_confirmed')->index();
            $table->bigInteger('created_at')->index();
            $table->bigInteger('updated_at')->index();
            $table->integer('role')->index('role_id');
        });

        $this->schema()->create($this->prefixModule.'email_confirm', function(Blueprint $table) {
            $table->bigIncrements('record_id');
            $table->bigInteger('user_id')->index('user_id');
            $table->string('code', 80);
            $table->bigInteger('time_life')->index('limit');
        });

        $this->schema()->create($this->prefixModule.'password_reset', function(Blueprint $table) {
            $table->bigIncrements('record_id');
            $table->bigInteger('user_id')->index('user_id');
            $table->string('code', 80);
            $table->bigInteger('time_life')->index('limit');
        });

        $this->schema()->create($this->prefixModule.'failed_signin', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->index();
            $table->string('agent',256)->index();
            $table->tinyInteger('count');
            $table->bigInteger('created_at')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema()->dropIfExists($this->prefixModule.'users');
        $this->schema()->dropIfExists($this->prefixModule.'email_confirm');
        $this->schema()->dropIfExists($this->prefixModule.'password_reset');
        $this->schema()->dropIfExists($this->prefixModule.'failed_signin');
    }
}
