<?php

use MMV\FW\Example\Db\Migration;
use Illuminate\Database\Schema\Blueprint;

class SessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema()->create('sessions', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->binary('data');
            $table->bigInteger('user_id')->index();
            $table->bigInteger('time_life')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema()->dropIfExists('sessions');
    }
}
