#!/usr/bin/env php
<?php

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Console\Application;

require_once(__DIR__.'/../vendor/autoload.php');

(new Dotenv())->loadEnv(__DIR__.'/../config/.env');
$config = require(__DIR__.'/../config/config.php');

date_default_timezone_set($config['locale']['timezone']);

set_time_limit(0);

$app = new App\Example($config);

$console = new Application();

/**
 * register commands
 */
$console->add(new MMV\FW\Example\Console\AppKeyGenerate('key-generate', $config));

$console->add(new MMV\FW\Example\Console\MigrateRun('migrate:run', $app));
$console->add(new MMV\FW\Example\Console\MigrateRollback('migrate:rollback', $app));
$console->add(new MMV\FW\Example\Console\MigrateMake('migrate:make', $app));

$console->add(new MMV\FW\Example\Console\NewUser('new-user', $app));

$console->add(new MMV\FW\Example\Console\ClearAuth('clear:auth', $app));
$console->add(new MMV\FW\Example\Console\ClearOldSession('clear:session', $app));
$console->add(new MMV\FW\Example\Console\ClearCache('clear:cache', $app));
$console->add(new MMV\FW\Example\Console\ClearDirectory('clear:email',     $app, 'Delete logs email.',         $app->config['email']['log']['path']));
$console->add(new MMV\FW\Example\Console\ClearDirectory('clear:logs',      $app, 'Clear logs of application.', $app->config['logger']['path']));
$console->add(new MMV\FW\Example\Console\ClearDirectory('clear:templates', $app, 'Clear templates.',           $app->config['template']['cache']));

$console->run();
