<?php

namespace MMV\FW\Example\Services;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\AbstractAdapter;

trait CacheTrait
{
    public function cache(): AbstractAdapter
    {
        if(is_null($this->cache)) {
            $this->cache = new FilesystemAdapter('',
                $this->config['cache']['timeDefault'],
                $this->config['__DIR__'].$this->config['cache']['path']);
        }
        return $this->cache;
    }

    private ?AbstractAdapter $cache = null;
}
