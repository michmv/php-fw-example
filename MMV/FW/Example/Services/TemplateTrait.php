<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Template;
use Twig\TwigFunction;
use Twig\TwigFilter;

trait TemplateTrait
{
    private ?Template $template = null;

    public function template(): Template
    {
        if(is_null($this->template)) {
            $logger = ($this->config['template']['logger']) ? $this->logger() : null;
            $this->template = new Template($this->config, $logger);

            // add helper service to every template
            $this->template()->twig->addGlobal('helper', $this->helper());

            $app = $this;
            $function = new TwigFunction('widget', function($class, $parameters=[]) use($app) {
                $class = str_replace('.', '\\', $class);
                $widget = new $class($app, $parameters);
                return (string)$widget;
            });
            $this->template->twig->addFunction($function);

            // add fillter trans
            $locale = $this->locale();
            $filter = new TwigFilter('trans', function ($id, $parameters=[], $domain='messages') use($locale) {
                return $locale->trans($id, $parameters, $domain);
            });
            $this->template->twig->addFilter($filter);

            // You can configuration twig
            $this->eventDispatcher()->initTwig($this->template->twig);
        }
        return $this->template;
    }
}
