<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Validator;
use MMV\FW\Example\Validator\TranslatorProvider;

trait ValidatorTrait
{
    private ?Validator $validator = null;

    public function validator(): Validator
    {
        if(is_null($this->validator)) {
            $provider = new TranslatorProvider($this->locale());
            $this->validator = new Validator($provider);
        }
        return $this->validator;
    }
}
