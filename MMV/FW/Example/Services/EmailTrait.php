<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Email\Email;
use MMV\FW\Example\Email\LogTransport;

trait EmailTrait
{
    public function email(): Email
    {
        if(is_null($this->email)) {
            $transport = $this->config['email']['transport'];

            switch($transport) {
                case 'log':
                    $path = $this->config['__DIR__'] . $this->config['email'][$transport]['path'];
                    $transport = new LogTransport($path);
                    break;
                default:
                    throw new \RuntimeException('Unknown transport for email');
            }

            $this->email = new Email($transport);
        }
        return $this->email;
    }

    protected ?Email $email = null;
}
