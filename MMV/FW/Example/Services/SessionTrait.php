<?php

namespace MMV\FW\Example\Services;

use MMV\Auth\Low\Session;
use MMV\FW\Example\Session\Security;
use MMV\FW\Example\Session\Environment;
use MMV\FW\Example\Auth\Storage;

trait SessionTrait
{
    public function session(): Session
    {
        if(is_null($this->session)) {
            $config = $this->config['session'];

            $security = new Security($this->security());
            $storage = new Storage($this->db(), $config['nameTable']);
            $environment = new Environment($this->request());

            $this->session = new Session($security, $storage, $environment, [
                'name' => $config['nameCookieVar'],
                'duration' => $config['duration'],
                'cookie' => $this->config['cookie'],
            ]);

            $environment->setHeaders($this->response()->headers);
        }
        return $this->session;
    }

    private ?Session $session = null;
}
