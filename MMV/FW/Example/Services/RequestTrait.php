<?php

namespace MMV\FW\Example\Services;

use Symfony\Component\HttpFoundation\Request;

trait RequestTrait
{
    public function request(): Request
    {
        if(is_null($this->request)) {
            $this->request = Request::createFromGlobals();
            $this->request->setDefaultLocale($this->config['locale']['default']);
        }
        return $this->request;
    }

    private ?Request $request = null;
}
