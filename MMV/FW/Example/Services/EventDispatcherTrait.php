<?php

namespace MMV\FW\Example\Services;

use App\EventDispatcher;

trait EventDispatcherTrait
{
    public function eventDispatcher(): EventDispatcher
    {
        if(is_null($this->eventDispatcher)) {
            $this->eventDispatcher = new EventDispatcher($this);
        }
        return $this->eventDispatcher;
    }

    private $eventDispatcher = null;
}

