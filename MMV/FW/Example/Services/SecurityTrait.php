<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Security;

trait SecurityTrait
{
    public function security(): Security
    {
        if(is_null($this->security)) {
            $this->security = new Security($this->config['security']);
        }
        return $this->security;
    }

    private ?Security $security = null;
}
