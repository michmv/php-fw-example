<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Helper;

trait HelperTrait
{
    private ?Helper $helper = null;

    public function helper(): Helper
    {
        if(is_null($this->helper)) {
            $this->helper = new Helper($this);
        }
        return $this->helper;
    }
}
