<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\ResponseGenerator;

trait ResponseTrait
{
    public function response(): ResponseGenerator
    {
        if(is_null($this->response)) {
            $this->response = new ResponseGenerator(
                $this->request(),
                $this->template(),
                $this->helper()->url,
                $this->session()
                );
        }
        return $this->response;
    }

    private ?ResponseGenerator $response = null;
}
