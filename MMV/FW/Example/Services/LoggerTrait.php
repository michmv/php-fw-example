<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use MMV\FW\Example\Monolog\AddTraceProcessor;
use MMV\FW\Example\Monolog\ConsoleJSHandler;
use MMV\FW\Example\Monolog\HttpHeaderHandler;
use MMV\FW\Example\Monolog\ExampleDebugBar;
use MMV\FW\Example\Monolog\PhpDebugBarHandler;

trait LoggerTrait
{
    private ?Logger $logger = null;

    public function logger():Logger
    {
        if(is_null($this->logger)) {

            $this->logger = new Logger('main');

            // in console
            $messageType = \in_array(\PHP_SAPI, ['cli', 'phpdbg'], true);
            $handler = new ErrorLogHandler(($messageType) ? ErrorLogHandler::SAPI : ErrorLogHandler::OPERATING_SYSTEM, Logger::ERROR);
            $handler->pushProcessor([AddTraceProcessor::class, 'addTrace']);
            $this->logger->pushHandler($handler);

            // in log file
            $handler = new StreamHandler(
                $this->config['__DIR__'].$this->config['logger']['path'].'/fw-'.date('Y-m-d').'.log',
                Logger::ERROR);
            $handler->pushProcessor([AddTraceProcessor::class, 'addTrace']);
            $this->logger->pushHandler($handler);

            // FirePHP
            if($this->config['debug'] && $this->config['logger']['firePhp']) {
                $this->logger->pushHandler(new FirePHPHandler(Logger::DEBUG));
            }

            // js.console.log
            if($this->config['debug'] && $this->config['logger']['jsConsoleLog']) {
                $this->logger->pushHandler(new ConsoleJSHandler($this->helper()->head, Logger::DEBUG));
            }

            // http header
            if($this->config['debug'] && $this->config['logger']['httpHeaderLog']) {
                $this->logger->pushHandler(new HttpHeaderHandler($this->response()->headers, Logger::DEBUG));
            }

            if($this->config['debug'] && $this->config['logger']['phpDebugBar']) {
                $this->logger->phpDebugBar = new ExampleDebugBar($this->config);
                $this->logger->pushHandler(new PhpDebugBarHandler($this->logger->phpDebugBar, Logger::DEBUG));
            }
        }
        return $this->logger;
    }
}
