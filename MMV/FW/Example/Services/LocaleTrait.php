<?php

namespace MMV\FW\Example\Services;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use MMV\FW\Example\Translator;

trait LocaleTrait
{
    private ?Translator $locale = null;

    public function locale(): Translator
    {
        if(is_null($this->locale)) {
            $this->locale = new Translator($this->request()->getLocale());
            $this->locale->addLoader('php', new PhpFileLoader);

            $resurces =
                self::getResourcesPhpTransTrait($this->config['__DIR__'], $this->config['locale']['directories']);
            
            foreach($resurces as $locale =>  $list) {
                foreach($list as $it) {
                    $this->locale->addResource('php', $it[1], $locale, $it[0]);
                }
            }
        }
        return $this->locale;
    }

    private static function getResourcesPhpTransTrait($root, $paths): array
    {
        $res = [];

        foreach($paths as $path) {
            $finder = new Finder;
            $finder->files()->depth('== 1')->name('*.php')->in($root.$path);

            foreach($finder as $file) {
                if($t = preg_match('/^.*\/(.+)\/(.+)\..*$/', $file, $matches)) {
                    $res[$matches[1]][] = [$matches[2], (string)$file];
                }
            }
        }

        return $res;
    }
}
