<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Routes;
use Symfony\Component\Routing\RouteCollection;

trait RoutesTrait
{
    public function routes(): Routes
    {
        if(is_null($this->routes)) {
            $routes = new Routes(new RouteCollection(), $this->request());

            $routes = $this->eventDispatcher()->setRoutes($routes);

            $this->routes = $routes;
        }
        return $this->routes;
    }

    private ?Routes $routes = null;
}
