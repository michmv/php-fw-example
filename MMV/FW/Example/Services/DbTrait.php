<?php

namespace MMV\FW\Example\Services;

use MMV\FW\Example\Database;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

trait DbTrait
{
    public function db(): Database
    {
        if(is_null($this->db)) {
            $this->db = new Database();
            $this->db->addConnection($this->config['database']['connect']);
            $this->db->setAsGlobal();
            $this->db->bootEloquent();
            $this->db->setEventDispatcher(new Dispatcher(new Container));

            if($this->config['debug'] && $this->config['database']['logger']) {
                // logger sql query
                $this->db->listen(function ($query) {
                    $this->logger()->debug('DATABASE', [
                        'sql' => $query->sql,
                        'binds' => $query->bindings,
                        'time' => $query->time . ' ms',
                        ]);
                    });
            }
        }
        return $this->db;
    }

    private ?Database $db = null;
}
