<?php

namespace MMV\FW\Example\Services;

use MMV\Auth\Low\Validator;
use MMV\FW\Example\Auth\Auth;
use MMV\Auth\Low\Access;
use MMV\FW\Example\Auth\Storage;
use MMV\FW\Example\Auth\Security;
use MMV\FW\Example\Auth\Environment;

trait AuthTrait
{
    private ?Auth $auth = null;

    public function auth(): Auth
    {
        if(is_null($this->auth)) {
            $storage = new Storage($this->db());
            $validator = new Validator($storage);
            $security = new Security($this->security());
            $session = $this->session();
            $environment = new Environment($this->request());

            $accessConfig = require($this->config['__DIR__'].$this->config['auth']['pathToAccess']);
            $access = new Access($accessConfig['roles'], $accessConfig['resources']);

            $this->auth = new Auth($validator, $storage, $security, $session, $environment, $access);
            $this->auth->defaulRoleId = $this->config['auth']['defaultRoleId'];
        }
        return $this->auth;
    }
}
