<?php

namespace MMV\FW\Example\Backend;

use MMV\FW\Example\Widget;

class ViewInfoRole extends Widget
{
    public $role;

    public function __toString()
    {
        $role = $this->role;
        $h = $this->app->helper();

        $res = [];
        if($role->title) $res[] = '<strong>'.$h->escape($h->trans($role->title)).'</strong>';
        if($role->description) $res[] = $h->escape($h->trans($role->description));

        return implode('<br>', $res);
    }
}
