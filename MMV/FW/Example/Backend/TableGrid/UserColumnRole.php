<?php

namespace MMV\FW\Example\Backend\TableGrid;

use MMV\FW\Example\Widgets\TableGrid\Column;

class UserColumnRole extends Column
{
    public $roles = [];

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @param array $viewParams
     * @return string
     */
    public function htmlCellBody($helper, $row, $index, $viewParams)
    {
        $field = $this->field;
        $field = $row->$field;

        $value = array_key_exists($field, $this->roles) ?
            $this->roles[$field] : '-';

        if($this->translate) $value = $helper->trans($value);
        if($this->escape) $value = $helper->escape($value);

        return '<td class="n'.$index.'">'.$value.'</td>';
    }
}
