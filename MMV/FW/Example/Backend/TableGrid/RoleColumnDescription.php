<?php

namespace MMV\FW\Example\Backend\TableGrid;

use MMV\FW\Example\Widgets\TableGrid\Column;

class RoleColumnDescription extends Column
{
    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @param array $viewParams
     * @return string
     */
    public function htmlCellBody($helper, $row, $index, $viewParams)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->trans($field);
        if($this->escape) $field = $helper->escape($field);

        $title = $row->title;
        if($this->translate) $title = $helper->trans($title);
        if($this->escape) $title = $helper->escape($title);

        return '<td class="n'.$index.'"><strong>'.$title.'</strong><br>'.$field.'</td>';
    }
}
