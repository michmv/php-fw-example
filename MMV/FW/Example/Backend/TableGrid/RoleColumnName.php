<?php

namespace MMV\FW\Example\Backend\TableGrid;

use MMV\FW\Example\Widgets\TableGrid\Column;

class RoleColumnName extends Column
{
    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @param array $viewParams
     * @return string
     */
    public function htmlCellBody($helper, $row, $index, $viewParams)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->trans($field);
        if($this->escape) $field = $helper->escape($field);
        return '<td class="n'.$index.'"><a href="'.$helper->url->route('backend.user.role.view', ['id'=>$row->id]).'">'.$field.'</a></td>';
    }
}
