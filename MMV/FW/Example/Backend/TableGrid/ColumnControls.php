<?php

namespace MMV\FW\Example\Backend\TableGrid;

use MMV\FW\Example\Widgets\TableGrid\Column;
use MMV\FW\Example\Utility;

class ColumnControls extends Column
{
    public $buttons = [];

    public $defaultButton = \MMV\FW\Example\Backend\Types\ButtonTableGrid::class;

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @param array $viewParams
     * @return string
     */
    public function htmlCellBody($helper, $row, $index, $viewParams)
    {
        $buttons = Utility::toObjectList($this->buttons, $this->defaultButton);

        $res = [];
        foreach($buttons as $button) {
            $res[] = $button->htmlButton($helper, $row, $index);
        }

        return '<td width="1px" class="n'.$index.'">'.($res ? '<span class="text-nowrap table_grid_controls">'.implode('', $res).'</span>' : '' ).'</td>';
    }
}
