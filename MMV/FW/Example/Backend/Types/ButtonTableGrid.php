<?php

namespace MMV\FW\Example\Backend\Types;

class ButtonTableGrid
{
    /**
     * @var string
     */
    public $id = '';

    /**
     * @var string
     */
    public $icon = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var callable
     */
    public $url;

    public function __construct($icon='')
    {
        $this->icon = $icon;
        $this->url = function() { return '#'; };
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlButton($helper, $row, $index)
    {
        $title = $this->title ? ' title="'.$helper->escape($this->title).'"' : '';
        $id = $this->id ? ' class="'.$this->id.'"' : '';

        return '<a'.$title.$id.' data-id="'.$row->id.'" href="'.call_user_func_array($this->url, [$helper, $row, $index]).'"><i class="far '.$this->icon.'"></i></a>';
    }
}
