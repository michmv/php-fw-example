<?php

namespace MMV\FW\Example\Validator;

use Illuminate\Contracts\Translation\Translator as IlluminateTranslator;
use MMV\FW\Example\Translator as ExampleTranslator;

class TranslatorProvider implements IlluminateTranslator
{
    protected ExampleTranslator $translator;

    public function __construct(ExampleTranslator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Get the translation for a given key.
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  string|null  $locale
     * @return mixed
     */
    public function get($key, array $replace = [], $locale = null)
    {
        if(substr($key, 0, 11) == 'validation.') {
            $domain = 'validation';
            $id = substr($key, 11);
        } else {
            $domain = null;
            $id = $key;
        }

        $res = $this->translator->trans($id, $replace, $domain, $locale);

        if($res == $id) return $key; // not translate
        else return $res;
    }

    /**
     * Get a translation according to an integer value.
     *
     * @param  string  $key
     * @param  \Countable|int|array  $number
     * @param  array  $replace
     * @param  string|null  $locale
     * @return string
     */
    public function choice($key, $number, array $replace = [], $locale = null)
    {
        throw new \RuntimeException('Woops!!!');
    }

    /**
     * Get the default locale being used.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->translator->getLocale();
    }

    /**
     * Set the default locale.
     *
     * @param  string  $locale
     * @return void
     */
    public function setLocale($locale)
    {
        $this->translator->setLocale($locale);
    }
}
