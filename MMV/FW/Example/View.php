<?php

namespace MMV\FW\Example;

use MMV\FW\Example\Template;

class View
{
    public string $path;

    public array $parameters;

    protected Template $template;

    public function __construct(Template $template, string $path, array $parameters)
    {
        $this->template = $template;
        $this->path = $path;
        $this->parameters = $parameters;
    }

    public function __toString()
    {
        return $this->template->render($this->path, $this->parameters);
    }
}
