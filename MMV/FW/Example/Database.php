<?php

namespace MMV\FW\Example;

use Illuminate\Database\Capsule\Manager;

class Database extends Manager
{
    public function __call($method, $parameters) {
        $t = static::connection();
        return static::connection()->$method(...$parameters);
    }
}
