<?php

namespace MMV\FW\Example\Session;

use MMV\Auth\Low\Session\SecurityInterface;
use MMV\FW\Example\Security as SecurityService;
use Throwable;

class Security implements SecurityInterface
{
    protected SecurityService $security;

    public function __construct(SecurityService $security)
    {
        $this->security = $security;
    }

    public function encrypt(string $str): string
    {
        return $this->security->crypt->encryptString($str);
    }

    public function decrypt(string $str): string
    {
        try {
            return $this->security->crypt->decryptString($str);
        }
        catch (Throwable $e) {
            return '';
        }
    }

    public function uuid(): string
    {
        return str_replace('-', '', $this->security->uuid());
    }
}
