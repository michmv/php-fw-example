<?php

namespace MMV\FW\Example\Session;

use MMV\Auth\Low\Session\EnvironmentInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

class Environment implements EnvironmentInterface
{
    protected ?ResponseHeaderBag $headers = null;

    protected Request $request;

    public function __construct(Request $request, ?ResponseHeaderBag $headers=null)
    {
        $this->request = $request;
        $this->header = $headers;
    }

    public function setCookie(string $name, string $value = null, $expire = 0, ?string $path = '/', string $domain = null, bool $secure = null, bool $httpOnly = true)
    {
        $this->headers->setCookie(Cookie::create($name, $value, $expire, $path, $domain, $secure, $httpOnly));
    }

    public function getCookie(string $name, string $default='')
    {
        return $this->request->cookies->get($name, $default);
    }

    public function setHeaders(ResponseHeaderBag $headers)
    {
        $this->headers = $headers;
    }

    public function getHeaders(): ResponseHeaderBag
    {
        return $this->headers;
    }
}
