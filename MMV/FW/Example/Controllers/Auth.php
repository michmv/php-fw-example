<?php

namespace MMV\FW\Example\Controllers;

use MMV\FW\Example\Routes;
use MMV\FW\Example\Validator;
use MMV\FW\Example\Auth\Trans;
use MMV\Auth\Low\Auth as AuthModule;
use MMV\Auth\Low\User;
use MMV\FW\Example\Controllers\BaseAbstract;
use MMV\FW\Example\Middleware\RobotDetector;

class Auth extends BaseAbstract
{
    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     * 
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters)
    {
        if($t = RobotDetector::handler($this->app->request())) return $t;
    }

    /**
     * SignIn for users
     */
    public function signin()
    {
        // check guest
        if(!$this->app->auth()->isGuest()) return $this->redirectHome();

        $session = $this->app->session();

        $view = 'auth/signin.html.twig';
        $data = [
            'data' => [],
            'errors' => [],
            'message' => ($session->has('message') ? $session->get('message') : ''),
        ];

        // POST
        if($this->app->request()->getMethod() == 'POST')
            return $this->postSignin($view, $data);

        if($session->has('email'))
            $data['data']['email'] = $session->get('email');

        return $this->view($view, $data);
    }

    /**
     * SignUp new user
     */
    public function signup()
    {
        // check turn on registration
        if(!$this->config('auth.registration'))
            return $this->abort(404);

        // check guest
        if(!$this->app->auth()->isGuest()) return $this->redirectHome();

        $view = 'auth/signup.html.twig';

        // POST
        if($this->app->request()->getMethod() == 'POST')
            return $this->postSignup($view);

        return $this->view($view, [
            'data' => [],
            'errors' => [],
        ]);
    }

    /**
     * Request for reset password
     */
    public function forgotPassword()
    {
        // check guest
        if(!$this->app->auth()->isGuest()) return $this->redirectHome();

        $data = ['data'=>[], 'errors'=>[]];
        $view = 'auth/forgot_password.html.twig';

        // POST
        if($this->app->request()->getMethod() == 'POST')
            return $this->postForgotPassword($view);

        return $this->view($view, $data);
    }

    /**
     * Confirm email
     */
    public function confirmEmail($id, $code)
    {
        if($user = $this->app->auth()->confirmEmail($id, $code)) {
            return $this->redirect()
                ->route('auth.signin')
                ->flash('message', $this->trans('auth@messages.successConfirmEmail'))
                ->flash('email', $user->email)
                ;
        }
        return $this->abort(404);
    }

    /**
     * Reset new password
     */
    public function resetPassword($id, $code)
    {
        $auth = $this->app->auth();
        if(!$auth->isGuest())
            return $this->redirect()->route('auth.change.password');

        if($user = $auth->findUserByRecordResetPassword($id, $code)) {
            return $this->changePasswordFunction($auth, $user, $id, true);
        }

        return $this->abort(404);
    }

    /**
     * Change password for user
     */
    public function changePassword()
    {
        $auth = $this->app->auth();
        if($auth->isGuest()) return $this->abort(403);

        return $this->changePasswordFunction($auth, $auth->user(), 0, false);
    }

    /**
     * SignOut for user
     */
    public function signout()
    {
        if(!$this->app->auth()->isGuest()) {
            $this->app->auth()->signOut();
        }

        return $this->redirectHome();
    }

    /**
     * Close all other sessions for user
     */
    public function signoutAll()
    {
        if(!$this->app->auth()->isGuest()) {
            $this->app->auth()->signOUtAll($this->app->auth()->user());
        }

        return $this->redirectHome();
    }

    /**
     * List routes for this controller
     */
    public static function routes(Routes $routes, $prefix)
    {
        $routes->add('auth.signin', $prefix.'/signin')
            ->controller(get_called_class(), 'signin');
        
        $routes->add('auth.signout', $prefix.'/signout')
            ->controller(get_called_class(), 'signout');

        $routes->add('auth.signout.all', $prefix.'/signout/all')
            ->controller(get_called_class(), 'signoutAll');

        $routes->add('auth.signup', $prefix.'/signup')
            ->controller(get_called_class(), 'signup');

        $routes->add('auth.forgot.password', $prefix.'/forgot/password')
            ->controller(get_called_class(), 'forgotPassword');

        $routes->add('auth.confirm.email', $prefix.'/confirm/email/{id}/{code}')
            ->controller(get_called_class(), 'confirmEmail');

        $routes->add('auth.reset.password', $prefix.'/reset/password/{id}/{code}')
            ->controller(get_called_class(), 'resetPassword');

        $routes->add('auth.change.password', $prefix.'/change/password')
            ->controller(get_called_class(), 'changePassword');
    }

    protected function changePasswordFunction(AuthModule $auth, User $user, $recordId=0, $confirmEmail)
    {
        $view = 'auth/change_password.html.twig';
        $data = ['data'=>[], 'errors'=>[]];

        // POST
        if($this->app->request()->getMethod() == 'POST')
            return $this->postChangePassword($view, $user, $recordId, $auth, $confirmEmail);
    
        $data['user'] = $user;
        return $this->view($view, $data);
    }

    protected function postChangePassword($view, User $user, $recordId, AuthModule $auth, $confirmEmail)
    {
        $request = $this->app->request()->request;
        $newPassword = $request->get('password', '');
        $confirmPassword = $request->get('confirm', '');

        if($auth->changePasswordForUser($user, $newPassword, $confirmPassword, $recordId, $confirmEmail)) {
            $this->app->session()->regenerate();

            // redirect to login form
            return $this->redirect()
                ->route('auth.signin')
                ->flash('message', $this->trans('auth@messages.passwordHasBeenChanged'));
        }

        return $this->view($view, [
            'data' => [],
            'errors' => $this->convertErrorMessage($auth->getMessages()),
            'user' => $user,
        ]);
    }

    protected function postForgotPassword($view)
    {
        $email = $this->app->request()->request->get('uid', '');
        $auth = $this->app->auth();

        $user = $auth->findUserByEmail($email);
        if($user) {
            $auth->beginTransaction();

            // create record for reset password
            list($recordId, $code) = $auth->createResetPassword($user);

            // send email
            $this->sendEmail('email.reset.password.html.twig', 'auth@subjectForgotPassword',
                ['user'=>$user, 'record_id'=>$recordId, 'code'=>$code]);

            $auth->commitTransaction();

            // redirect with message
            return $this->redirect()->route('auth.signin')
                ->flash('message', $this->trans('auth@messages.createResetPassword'));
        }

        $data = [
            'data'=>['email'=>$email],
            'errors'=>$this->convertErrorMessage($auth->getMessages())
            ];
        return $this->view($view, $data);
    }

    protected function postSignin($view, $data)
    {
        $request = $this->app->request()->request;
        $auth = $this->app->auth();

        $email      = $request->get('uid', '');
        $password   = $request->get('password', '');
        $rememberme = $request->getInt('rememberme', 0);

        $auth->beginTransaction();

        $user = $auth->signIn($email, $password);
        if($user) {
            // check confirmed email
            if(!$this->config('auth.signinOnlyAfterConfirmedEmail') || $user->email_confirmed) {
                // write session
                $auth->createSession($user, (bool)$rememberme);

                $auth->commitTransaction();

                // redirect
                return $this->redirectHome();
            }
            else {
                // need confirme email
                $auth->addMessage('form', 'email_not_confirm');
            }
        }

        $auth->commitTransaction();

        // show error
        $data['data'] = ['email' => $email, 'rememberme'=>$rememberme];
        $data['errors'] = $this->convertErrorMessage($auth->getMessages());
        return $this->view($view, $data);
    }

    protected function postSignup($view)
    {
        $r = $this->app->request()->request;
        $data = [
            'email'    => $r->get('uid', ''),
            'login'    => $r->get('login', ''),
            'password' => $r->get('password', ''),
            'confirm'  => $r->get('confirm', ''),
        ];

        $auth = $this->app->auth();
        $auth->beginTransaction();

        $user = $auth->signUp($data);
        if($user) {
            // user update
            $user->email_confirmed = 0;
            $user->role = $this->config('auth.defaultRoleId');
            $user->save();

            // need send confirm email
            list($recordId, $code) = $auth->createEmailConfirm($user);
            $this->sendEmail('email.confirm.html.twig', 'auth@subjectConfirm',
                ['user'=>$user, 'record_id'=>$recordId, 'code'=>$code]);

            $auth->commitTransaction();

            // redirect with message
            $redirect = $this->redirect()->route('auth.signin');
            $message = $this->config('auth.signinOnlyAfterConfirmedEmail') ?
                'auth@messages.successRegistration' :
                'auth@messages.successRegistrationWithOutEmailConfirmed' ;
            $redirect->flash('message', $this->trans($message));
            return $redirect;
        }

        $auth->rollbackTransaction();

        return $this->view($view, [
            'data' => $data,
            'errors' => $this->convertErrorMessage($auth->getMessages()),
        ]);
    }

    protected function sendEmail(string $view, string $subject, array $data)
    {
        $view_ = 'auth/' . $this->app->locale()->getLocale() . '.' .$view;
        $config = $this->config('email');

        $email = $this->app->email();
        $message = $email->message;
        $message->setSubject($this->trans($subject))
            ->setFrom($config['from']['address'], $config['from']['name'])
            ->setTo($data['user']->email)
            ->setBody($this->app->template()->render($view_, $data));
        $email->send();
    }

    protected function redirectHome()
    {
        $config = $this->config('auth');

        $nameRoute = $this->app->auth()->check($config['accessToBackend']) ?
            $config['nameRoute']['backend'] :
            $config['nameRoute']['home'];

        return $this->redirect()->route($nameRoute);
    }

    protected function convertErrorMessage(array $errors)
    {
        return Validator::makeMessageBag(Trans::trans($this->app->locale(), $errors));
    }
}
