<?php

namespace MMV\FW\Example\Controllers;

use MMV\FW\Example\Controllers\BackendAbstract;

class Dashboard extends BackendAbstract
{
    public function dashboard()
    {
        return $this->view('dashboard.html.twig');
    }
}
