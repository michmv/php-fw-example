<?php

namespace MMV\FW\Example\Controllers;

use MMV\FW\Example\Controllers\BaseAbstract;

abstract class BackendAbstract extends BaseAbstract
{
    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     *
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters)
    {
        if(!$this->app->auth()->check('backend'))
            return $this->app->response()->abort(403);
    }
}
