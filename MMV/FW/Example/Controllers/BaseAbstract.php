<?php

namespace MMV\FW\Example\Controllers;

abstract class BaseAbstract
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param \App\Example $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     * 
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters)
    {
        // ...
        return null;
    }

    /**
     * @param mixed $response
     * @return mixed
     */
    public function terminate($response)
    {
        // ...
        return $response;
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return \MMV\FW\Example\Response
     */
    protected function view(string $path, array $parameters = [])
    {
        return $this->app->response()->view($path, $parameters);
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    protected function config(string $name, $default=null)
    {
        $path = explode('.', $name);
        $config = $this->app->config;
        foreach($path as $key) {
            if(array_key_exists($key, $config)) {
                $config =& $config[$key];
            }
            else {
                return $default;
            }
        }
        return $config;
    }

    /**
     * @param string $url
     * @param int $code
     * @return \MMV\FW\Example\Redirect
     */
    protected function redirect(string $url='', int $code=302)
    {
        return $this->app->response()->redirect($url, $code);
    }

    /**
     * @param int $code
     * @param string $message
     * @return \MMV\FW\Example\Response
     */
    protected function abort(int $code, string $message='')
    {
        return $this->app->response()->abort($code, $message);
    }

    /**
     * @param string|null $id
     * @param array $parameters
     * @param string $domain
     * @param string $locale
     * @return mixed
     */
    protected function trans(?string $id, array $parameters=[], string $domain=null, string $locale=null)
    {
        return $this->app->locale()->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @return \MMV\FW\Example\UrlGenerator
     */
    protected function url()
    {
        return $this->app->helper()->url;
    }
}
