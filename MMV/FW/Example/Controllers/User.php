<?php

namespace MMV\FW\Example\Controllers;

use MMV\FW\Example\Controllers\BackendAbstract;
use MMV\FW\Example\Routes;
use MMV\FW\Example\Validator;
use MMV\FW\Example\Auth\Trans;

class User extends BackendAbstract
{
    /**
     * Show list roles
     */
    public function roleList()
    {
        $defaultIdRole = (int)$this->config('auth.defaultRoleId', 0);
        $roles = $this->app->auth()->access()->getRoles();

        return $this->view('user/role_list.html.twig', [
            'roleDefault' => $this->findNameRoleById($defaultIdRole, $roles),
            'roles' => $this->rolesToArray($roles),
        ]);
    }

    /**
     * View role detals
     */
    public function roleView($id)
    {
        $access = $this->app->auth()->access();

        $role = $access->findRoleById((int)$id);
        if(!$role) return $this->abort(404);

        $resources = $access->getResourcesGroup();

        return $this->view('user/role_view.html.twig', [
            'role' => $role,
            'resources' => $resources,
            'access' => $access,
        ]);
    }

    /**
     * Show list users
     */
    public function userList()
    {
        $users = $this->app->db()
            ->table('auth_users')
            ->select('id', 'email', 'login', 'role');

        $access = $this->app->auth()->access();

        return $this->view('user/user_list.html.twig',[
            'collection' => $users,
            'roles' => $this->listRolesTitle($access->getRoles()),
        ]);
    }

    /**
     * Form for create a new user
     */
    public function userCreate()
    {
        $view = 'user/user_create.html.twig';
        $roles = $this->listRolesTitle($this->app->auth()->access()->getRoles());

        if($this->app->request()->isMethod('POST'))
            return $this->postUserCreate($view, $roles);

        return $this->view($view, [
            'roles' => $roles,
        ]);
    }

    protected function postUserCreate($view, $roles)
    {
        $data = $this->importData($this->app->request());
        $data['password'] = $this->app->security()->randomString(16);
        $user = $this->app->auth()->makeUser($data);

        if($user->save()) {
            return $this->redirect()->route('backend.user.update', ['id'=>$user->id])
                ->flash('message', 'backend@messages.userCreated');
        }
        else {
            return $this->view($view, [
                'user' => $user,
                'roles' => $roles,
                'errors' => $this->convertErrorMessage($user->getMessages()),
            ]);
        }
    }

    /**
     * Form for update a user
     */
    public function userUpdate($id)
    {
        $user = $this->app->auth()->findUserById($id);
        if(!$user) return $this->abort(404);

        $view = 'user/user_update.html.twig';
        $roles = $this->listRolesTitle($this->app->auth()->access()->getRoles());

        if($this->app->request()->isMethod('POST'))
            return $this->postUserUpdate($view, $user, $roles);

        return $this->view($view, [
            'user' => $user,
            'roles' => $roles,
            'message' => $this->app->session()->get('message', ''),
        ]);
    }

    protected function postUserUpdate($view, $user, $roles)
    {
        $auth = $this->app->auth();
        $data = $this->importData($this->app->request());

        foreach($data as $key => $val) {
            $user->$key = $val;
        }

        $auth->beginTransaction();
        if($user->save()) {
            // ok
            $session = $this->app->session();
            $session->removeAllSessionForUser($user->id);
            $session->flash('message', 'backend@messages.userUpdated');
            $errors = [];
            $auth->commitTransaction();
        }
        else {
            // error
            $auth->rollbackTransaction();
            $errors = $this->convertErrorMessage($user->getMessages());
        }

        return $this->view($view, [
            'user' => $user,
            'roles' => $roles,
            'errors' => $errors,
            'message' => $this->app->session()->get('message', ''),
        ]);
    }

    /**
     * Delete a user
     */
    public function userDelete($id)
    {
        $auth = $this->app->auth();
        $user = $auth->findUserById($id);
        if($user){
            $auth->beginTransaction();
            $user->delete();
            $this->app->session()->removeAllSessionForUser($user->id);
            $auth->commitTransaction();
        }
    }

    public static function routes(Routes $routes, $prefix)
    {
        // Role

        $routes->add('backend.user.role.list', $prefix.'/users/roles/list')
            ->controller(get_called_class(), 'roleList');

        $routes->add('backend.user.role.view', $prefix.'/users/role/{id}')
            ->controller(get_called_class(), 'roleView');

        // User

        $routes->add('backend.user.list', $prefix.'/users/list')
            ->controller(get_called_class(), 'userList');

        $routes->add('backend.user.create', $prefix.'/users/create')
            ->controller(get_called_class(), 'userCreate');

        $routes->add('backend.user.update', $prefix.'/users/update/{id}')
            ->controller(get_called_class(), 'userUpdate');

        $routes->add('backend.user.delete', $prefix.'/users/delete/{id}')
            ->controller(get_called_class(), 'userDelete')
            ->method('POST');
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public static function genUrlUpdate($helper, $row, $index)
    {
        return $helper->url->route('backend.user.update', ['id' => $row->id]); 
    }

    protected function findNameRoleById($id, $roles)
    {
        foreach($roles as $role) {
            if((string)$role->id === (string)$id) return $role->name;
        }
        return 'unknown';
    }

    protected function rolesToArray($roles)
    {
        $res = [];
        foreach($roles as $role) {
            $res[] = ['id'=>$role->id, 'name'=>$role->name, 'title'=>$role->title(), 'description'=>$role->description];
        }
        return $res;
    }
    
    protected function listRolesTitle($roles)
    {
        $res = [];
        foreach($roles as $role) {
            $res[$role->id] = $this->trans($role->title());
        }
        return $res;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return array
     */
    protected function importData($request)
    {
        return [
            'email'           => $request->request->get('email', ''),
            'login'           => $request->request->get('login', ''),
            'email_confirmed' => $request->request->getInt('email_confirmed', 0),
            'role'            => $request->request->getInt('role', 0),
        ];
    }

    protected function convertErrorMessage(array $errors)
    {
        return Validator::makeMessageBag(Trans::trans($this->app->locale(), $errors));
    }
}
