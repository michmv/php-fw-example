<?php

namespace MMV\FW\Example;

use MMV\FW\Example\Routes;
use Twig\Environment;

abstract class EventDispatcherAbstract
{
    public $log = [];

    /**
     * @param \App\Example $app
     */
    protected $app;

    /**
     * @param \App\Example $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * You can set custom routes here
     */
    abstract public function setRoutes(Routes $routes): Routes;

    /**
     * You can to make custom twig here
     */
    public function initTwig(Environment $twig): Environment
    {
        return $twig;
    }

    /**
     * Type: [ 'first' => [ MMV\FW\Example\Backend\MenuItemTop ]
     *       , 'last'  => [ MMV\FW\Example\Backend\MenuItemTop ]
     *       ]
     *
     * @param array $list
     * @return array
     */
    public function buildBackendTopMenu(array $list): array
    {
        return $list;
    }

    /**
     * Type: [ 'first' => [ MMV\FW\Example\Backend\MenuItemLeft ]
     *       , 'last'  => [ MMV\FW\Example\Backend\MenuItemLeft ]
     *       ]
     *
     * @param array $list
     * @return array
     */
    public function buildBackendLeftMenu(array $list): array
    {
        return $list;
    }

    public function __call($name, $arguments)
    {
        $result = count($arguments) ? $arguments[0] : null;

        $trace = debug_backtrace();
        $log = ['event' => $name];
        foreach(['file', 'line', 'class', 'type'] as $it) {
            if(isset($trace[0][$it]))
                $log[$it] = $trace[0][$it];
        }

        $this->log[] = $log;

        return $result;
    }
}
