<?php

namespace MMV\FW\Example;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Generator\UrlGenerator as SymfonyUrlGenerator;
use Symfony\Component\HttpFoundation\Request;

class UrlGenerator
{
    public RequestContext $context;

    public SymfonyUrlGenerator $urlGenerator;

    public Request $request;

    public function __construct(Request $request, RouteCollection $collection, RequestContext $context)
    {
        $this->request = $request;
        $this->context = $context;
        $this->urlGenerator = new SymfonyUrlGenerator($collection, $context);
    }

    /**
     * @see https://symfony.com/doc/current/routing.html#generating-urls
     */
    public function route($name='', $parameters=[], bool $absolute=false, array $query=[], string $fragment=''): string
    {
        if(!$name)
            $name = $this->request->attributes->get('_route');

        $referenceType = ($absolute) ?
            SymfonyUrlGenerator::ABSOLUTE_URL :
            SymfonyUrlGenerator::ABSOLUTE_PATH;

        return $this->urlGenerator->generate($name, $parameters, $referenceType) .
            $this->generateQuery($query) .
            $this->generateFragment($fragment)
            ;
    }

    public function path(string $link, bool $absolute=false, array $query=[], string $fragment=''): string
    {
        return $this->home($absolute) . '/' .
            trim(str_replace('%2F', '/', urlencode($link)), "/") .
            $this->generateQuery($query) .
            $this->generateFragment($fragment)
            ;
    }

    public function current(bool $absolute=false, array $query=[], string $fragment=''): string
    {
        return $this->home($absolute) .
            $this->request->getPathInfo() .
            $this->generateQuery($query) .
            $this->generateFragment($fragment)
            ;
    }

    public function home(bool $absolute=false, array $query=[], string $fragment=''): string
    {
        $request = $this->request;
        if($absolute) {
            $prefix =
                $request->getScheme() . '://' .
                $request->getHost() .
                $this->getPort()
                ;
        }
        else {
            $prefix = '';
        }

        return $prefix . $request->getBaseUrl() .
            $this->generateQuery($query) . $this->generateFragment($fragment);
    }

    protected function getPort()
    {
        if('http' === $this->context->getScheme() && 80 !== $this->context->getHttpPort()) {
            $port = ':'.$this->context->getHttpPort();
        } else if('https' === $this->context->getScheme() && 443 !== $this->context->getHttpsPort()) {
            $port = ':'.$this->context->getHttpsPort();
        } else {
            $port = '';
        }
        return $port;
    }

    protected const QUERY_FRAGMENT_DECODED = [
        // RFC 3986 explicitly allows those in the query/fragment to reference other URIs unencoded
        '%2F' => '/',
        '%3F' => '?',
        // reserved chars that have no special meaning for HTTP URIs in a query or fragment
        // this excludes esp. "&", "=" and also "+" because PHP would treat it as a space (form-encoded)
        '%40' => '@',
        '%3A' => ':',
        '%21' => '!',
        '%3B' => ';',
        '%2C' => ',',
        '%2A' => '*',
    ];

    protected function generateFragment(string $fragment): string
    {
        return ($fragment !== '') ?
            '#'.strtr(rawurlencode($fragment), static::QUERY_FRAGMENT_DECODED) :
            '' ;
    }

    protected function generateQuery(array $data): string
    {
        if(!$data) return '';
        
        $query = http_build_query($data, '', '&', \PHP_QUERY_RFC3986);
        return '?'.strtr($query, static::QUERY_FRAGMENT_DECODED);
    }
}
