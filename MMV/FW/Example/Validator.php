<?php

namespace MMV\FW\Example;

use MMV\FW\Example\Validator\TranslatorProvider;
use Illuminate\Validation\Validator as IlluminateValidator;
use Illuminate\Support\MessageBag;

class Validator
{
    protected TranslatorProvider $translatorProvader;

    protected ?IlluminateValidator $validator = null;

    public function __construct(TranslatorProvider $translatorProvader)
    {
        $this->translatorProvader = $translatorProvader;
    }

    public function passes(array $data, array $rules, array $messages=[], array $customAttributes=[]): bool
    {
        $this->validator = new IlluminateValidator($this->translatorProvader, $data, $rules, $messages, $customAttributes);
        return $this->validator->passes();
    }

    public function fails(array $data, array $rules, array $messages=[], array $customAttributes=[]): bool
    {
        return ! $this->passes($data, $rules, $messages, $customAttributes);
    }

    public function getMessages(): MessageBag
    {
        if(is_null($this->validator))
            throw new \RuntimeException('The validator is not initialized');
            
        return $this->validator->getMessageBag();
    }

    public static function makeMessageBag(array $messages): MessageBag
    {
        return new MessageBag($messages);
    }
}
