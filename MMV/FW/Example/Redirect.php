<?php

namespace MMV\FW\Example;

use MMV\FW\Example\UrlGenerator;
use MMV\FW\Example\Response;
use MMV\Auth\Low\Session;

class Redirect
{
    public string $url = '';

    public int $code = 302;

    public Session $session;

    public Response $response;

    public UrlGenerator $urlGenerator;

    public function __construct(Session $session, Response $response, UrlGenerator $urlGenerator)
    {
        $this->session = $session;
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
    }

    public function route($name='', $parameters=[], array $query=[], string $fragment='')
    {
        $this->url = $this->urlGenerator->route($name, $parameters, true, $query, $fragment);
        return $this;
    }

    public function path(string $link, array $query=[], string $fragment='')
    {
        $this->url = $this->urlGenerator->path($link, true, $query, $fragment);
        return $this;
    }

    public function current(array $query=[], string $fragment='')
    {
        $this->url = $this->urlGenerator->current(true, $query, $fragment);
        return $this;
    }

    public function home(array $query=[], string $fragment='')
    {
        $this->url = $this->urlGenerator->home(true, $query, $fragment);
        return $this;
    }

    public function flash(string $name, $value)
    {
        $this->session->flash($name, $value);
        return $this;
    }

    public function __toString()
    {
        $this->response->setStatusCode($this->code)
            ->setTargetUrl($this->url);

        return (string)$this->response;
    }
}
