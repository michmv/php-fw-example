<?php

namespace MMV\FW\Example\Email;

use Swift_Transport;
use Swift_Mime_SimpleMessage;
use Swift_Events_EventListener;

class LogTransport implements Swift_Transport
{
    protected string $path = '';

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * Test if this Transport mechanism has started.
     *
     * @return bool
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * Start this Transport mechanism.
     */
    public function start() {}

    /**
     * Stop this Transport mechanism.
     */
    public function stop() {}

    /**
     * Check if this Transport mechanism is alive.
     *
     * If a Transport mechanism session is no longer functional, the method
     * returns FALSE. It is the responsibility of the developer to handle this
     * case and restart the Transport mechanism manually.
     *
     * @example
     *
     *   if (!$transport->ping()) {
     *      $transport->stop();
     *      $transport->start();
     *   }
     *
     * The Transport mechanism will be started, if it is not already.
     *
     * It is undefined if the Transport mechanism attempts to restart as long as
     * the return value reflects whether the mechanism is now functional.
     *
     * @return bool TRUE if the transport is alive
     */
    public function ping()
    {
        return true;
    }

    /**
     * Send the given Message.
     *
     * Recipient/sender data will be retrieved from the Message API.
     * The return value is the number of recipients who were accepted for delivery.
     *
     * This is the responsibility of the send method to start the transport if needed.
     *
     * @param string[] $failedRecipients An array of failures by-reference
     *
     * @return int
     */
    public function send(Swift_Mime_SimpleMessage $message, &$failedRecipients = null)
    {
        $file = 'email_' . date('Y-m-d_H:i:s');

        // get file name
        $n = 0;
        while(file_exists($file__ = $this->genFileName($file, $n))) {
            $n++;
        }

        $h = fopen($file__, 'w');
        fwrite($h, $message->toString());
        fclose($h);

        return 1;
    }

    /**
     * Register a plugin in the Transport.
     */
    public function registerPlugin(Swift_Events_EventListener $plugin) {}

    protected function genFileName(string $file, int $index)
    {
        return $this->path . DIRECTORY_SEPARATOR . $file . ($index ? " $index" : '') . '.txt';
    }
}
