<?php

namespace MMV\FW\Example\Email;

use Swift_Transport;
use Swift_Mailer;
use Swift_Message;

class Email
{
    public $message;

    protected Swift_Transport $transport;

    protected Swift_Mailer $mailer;

    public function __construct(Swift_Transport $transport)
    {
        $this->transport = $transport;
        $this->mailer = new Swift_Mailer($transport);
        $this->message = new Swift_Message();
    }

    public function send()
    {
        return $this->mailer->send($this->message);
    }
}
