<?php

namespace MMV\FW\Example\Middleware;

use Symfony\Component\HttpFoundation\Request;

class TrimParameters
{
    public static function handler(Request $request)
    {
        $request->query->replace(static::trimArray($request->query->all()));
        $request->request->replace(static::trimArray($request->request->all()));
        $request->cookies->replace(static::trimArray($request->cookies->all()));
    }

    protected static function trimArray($arr)
    {
        foreach($arr as $key => $val) {
            if(is_string($val)) {
                $arr[$key] = trim($val);
            }
            else if(is_array($val)) {
                $arr[$key] = static::trimArray($val);
            }
        }
        return $arr;
    }
}
