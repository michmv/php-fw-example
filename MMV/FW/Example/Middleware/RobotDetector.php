<?php

namespace MMV\FW\Example\Middleware;

use Symfony\Component\HttpFoundation\Request;

class RobotDetector
{
    public static function handler(Request $request): ?string
    {
        if($request->isMethod('post')) {
            if( (!$request->request->has('email') || $request->request->get('email') !== '') ||
                ($request->request->get('promise', '') !== 'I\'m not a robot')
                )
                return 'Glory to the robots!';
        }
        return null;
    }
}
