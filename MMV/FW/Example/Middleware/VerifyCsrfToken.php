<?php

namespace MMV\FW\Example\Middleware;

use Symfony\Component\HttpFoundation\Request;
use MMV\Auth\Low\Session;

class VerifyCsrfToken
{
    public static function handler(Request $request, Session $session, string $name='__csrf'): bool
    {
        if($_POST || $request->isMethod('post')) {
            $token = $request->get($name, '');
            if(!$token || $token !== static::getToken($session, $name)) {
                return false;
            }
        }
        return true;
    }

    public static function getToken(Session $session, $name)
    {
        $time = (int)$session->get($name.'_time_life', 0);
        if($time > time()) {
            return (string)$session->get($name, '');
        }
        return '';
    }
}
