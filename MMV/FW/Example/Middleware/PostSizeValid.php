<?php

namespace MMV\FW\Example\Middleware;

use Symfony\Component\HttpFoundation\Request;

class PostSizeValid
{
    public static function handler(Request $request): bool
    {
        $max = static::getPostMaxSize();

        if($max > 0 && $request->server->get('CONTENT_LENGTH') > $max) {
            return false;
        }

        return true;
    }

    protected static function getPostMaxSize(): int
    {
        if (is_numeric($postMaxSize = ini_get('post_max_size'))) {
            return (int) $postMaxSize;
        }

        $metric = strtoupper(substr($postMaxSize, -1));
        $postMaxSize = (int) $postMaxSize;

        switch ($metric) {
            case 'K':
                return $postMaxSize * 1024;
            case 'M':
                return $postMaxSize * 1048576;
            case 'G':
                return $postMaxSize * 1073741824;
            default:
                return $postMaxSize;
        }
    }
}
