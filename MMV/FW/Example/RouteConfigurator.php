<?php

namespace MMV\FW\Example;

use Symfony\Component\Routing\Route;

class RouteConfigurator
{
    public const CALLBACK = 1;

    public const CONTROLLER = 2;

    protected Route $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * @param callable $callback A callable or parseable pseudo-callable
     * @return self
     */
    public function callback($callback): self
    {
        $this->route->addDefaults(['_execute' => $callback, '_type' => static::CALLBACK]);
        return $this;
    }

    public function controller(string $class, string $method): self
    {
        $this->route->addDefaults(['_execute' => [$class, $method], '_type' => static::CONTROLLER]);
        return $this;
    }

    /**
     * @param string|array $methods
     * @return self
     */
    public function method($methods): self
    {
        $this->route->setMethods($methods);
        return $this;
    }

    public function requirements(array $requirements): self
    {
        $this->route->addRequirements($requirements);
        return $this;
    }

    public function defaults(array $defaults): self
    {
        $this->route->addDefaults($defaults);
        return $this;
    }

    public function host(string $pattern): self
    {
        $this->route->setHost($pattern);
        return $this;
    }

    /**
     * Sets the schemes (e.g. 'https') this route is restricted to.
     * So an empty array means that any scheme is allowed.
     *
     * @param string[] $schemes
     *
     * @return $this
     */
    public function schemes(array $schemes): self
    {
        $this->route->setSchemes($schemes);
        return $this;
    }
}
