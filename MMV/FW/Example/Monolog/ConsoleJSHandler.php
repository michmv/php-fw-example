<?php

namespace MMV\FW\Example\Monolog;

use Monolog\Handler\AbstractProcessingHandler;
use MMV\FW\Example\HeadGenerator;
use Monolog\Logger;

class ConsoleJSHandler extends AbstractProcessingHandler
{
    protected HeadGenerator $head;

    protected $n = 0;

    public function __construct(HeadGenerator $head, $level = Logger::DEBUG, bool $bubble = true)
    {
        $this->head = $head;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     */
    protected function write(array $record): void
    {
        $p1 = '"['.$record['level_name'].'] '.$record['message'].'"';
        $p2 = json_encode($record['context']);
        $raw = 'console.log('.$p1.', '.$p2.');';

        $this->head->addSource('logMessage'.$this->n, '<script>'.$raw.'</script>');

        $this->n++;
    }
}

