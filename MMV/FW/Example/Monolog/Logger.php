<?php

namespace MMV\FW\Example\Monolog;

use Monolog\Logger as BaseLogger;
use DebugBar\DebugBar;
use MMV\FW\Example\Helper;

class Logger extends BaseLogger
{
    public ?DebugBar $phpDebugBar = null;

    /**
     * @param Helper $helper
     * @param array $enable ["jquery", "fontawesome", "highlightjs"]
     */
    public function phpDebugBarRenderHead(Helper $helper, $enable=[])
    {
        if(!is_null($this->phpDebugBar)) {
            $renderer = $this->phpDebugBar->getJavascriptRenderer('', $helper->url->path('assets/panel-admin/debug_bar', true));

            $disable = ["jquery", "fontawesome", "highlightjs"];
            foreach($disable as $it) {
                if(!in_array($it, $enable)) $renderer->disableVendor($it);
            }

            list($cssCollection, $jsCollection) = $renderer->getAssets();
            $this->attachPhpDebugBarResourcesToHtml($cssCollection, '<link href="$1" rel="stylesheet">', 'phpDebugBarCss', $helper->head);
            $this->attachPhpDebugBarResourcesToHtml($jsCollection, '<script src="$1" type="text/javascript"></script>', 'phpDebugBarJs', $helper->head);
        }
    }

    public function phpDebugBarRenderBody()
    {
        if(!is_null($this->phpDebugBar)) {
            return $this->phpDebugBar->getJavascriptRenderer()->render();
        }
    }

    protected function attachPhpDebugBarResourcesToHtml($list, $str, $name, $head)
    {
        foreach($list as $key => $it) {
            $head->addSource($name.$key, str_replace('$1', $it, $str));
        }
    }
}

