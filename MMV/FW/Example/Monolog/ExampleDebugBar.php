<?php

namespace MMV\FW\Example\Monolog;

use DebugBar\DataCollector\MessagesCollector;
use DebugBar\DataCollector\PhpInfoCollector;
use DebugBar\DebugBar;
use MMV\FW\Example\Monolog\DebugBar\DatabaseCollector;
use MMV\FW\Example\Monolog\DebugBar\TemplateCollector;

class ExampleDebugBar extends DebugBar
{
    public function __construct(array $config)
    {
        $this->addCollector(new PhpInfoCollector());
        $this->addCollector(new MessagesCollector());

        if($config['database']['logger']) {
            $this->addCollector(new DatabaseCollector());
        }

        if($config['template']['logger']) {
            $this->addCollector(new TemplateCollector());
        }
    }
}

