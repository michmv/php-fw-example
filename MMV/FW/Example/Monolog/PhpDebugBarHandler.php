<?php

namespace MMV\FW\Example\Monolog;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use DebugBar\DebugBar;

class PhpDebugBarHandler extends AbstractProcessingHandler
{
    protected DebugBar $debugBar;

    public function __construct(DebugBar $debugBar, $level = Logger::DEBUG, bool $bubble = true)
    {
        $this->debugBar = $debugBar;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     */
    protected function write(array $record): void
    {
        $level = mb_strtolower($record['level_name']);

        if($record['message'] == 'DATABASE') {
            $this->debugBar['database']->addMessage($record['context'], $level);
        }
        else if($record['message'] == 'TEMPLATE') {
            $this->debugBar['template']->addMessage($record['context'], $level);
        }
        else {
            $this->debugBar['messages']->addMessage(['msg' => $record['message'], 'context'=>$record['context']], $level);
        }

        return;
    }
}

