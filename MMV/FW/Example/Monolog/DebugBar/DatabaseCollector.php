<?php

namespace MMV\FW\Example\Monolog\DebugBar;

use DebugBar\DataCollector\MessagesCollector;

class DatabaseCollector extends MessagesCollector
{
    public function getName()
    {
        return 'database';
    }
}

