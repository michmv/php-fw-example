<?php

namespace MMV\FW\Example\Monolog\DebugBar;

use DebugBar\DataCollector\MessagesCollector;

class TemplateCollector extends MessagesCollector
{
    public function getName()
    {
        return 'template';
    }
}

