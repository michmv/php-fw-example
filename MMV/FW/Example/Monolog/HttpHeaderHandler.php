<?php

namespace MMV\FW\Example\Monolog;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class HttpHeaderHandler extends AbstractProcessingHandler
{
    protected $n = 0;

    protected ResponseHeaderBag $headers;

    public function __construct(ResponseHeaderBag $headers, $level = Logger::DEBUG, bool $bubble = true)
    {
        $this->headers = $headers;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     */
    protected function write(array $record): void
    {
        $p1 = 'Log_'.$this->n.'_'.$record['level_name'];
        $p2 = json_encode(['msg' => $record['message'], 'context' => $record['context']]);
        $this->headers->set($p1, $p2);

        $this->n++;
    }
}

