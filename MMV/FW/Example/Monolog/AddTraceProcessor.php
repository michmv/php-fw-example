<?php

namespace MMV\FW\Example\Monolog;

use Throwable;

class AddTraceProcessor
{
    public static function addTrace($record)
    {
        if(isset($record['context'][0]) && $record['context'][0] instanceof Throwable) {
            $list = $record['context'][0]->getTrace();
            $record['extra']['trace'] = $list;
        }
        return $record;
    }
}