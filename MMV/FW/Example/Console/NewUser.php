<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use MMV\FW\Example\Auth\Trans;

class NewUser extends Command
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        $this->app = $app;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Create new user and set roleId 1 for he.')

            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('login', InputArgument::REQUIRED)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->app->auth()->beginTransaction();

        $user = $this->app->auth()->signUp([
            'email' => $input->getArgument('email'),
            'login' => $input->getArgument('login'),
            'password' => 'qwerty',
            'confirm' => 'qwerty',
        ]);

        if($user) {
            $user->email_confirmed = 1;
            $user->role = 1;
            if($user->save()) {
                $this->app->auth()->commitTransaction();

                $output->writeln('Create new user for administration:');
                $output->writeln('email: '.$user->email);
                $output->writeln('password: qwerty');
                $output->writeln('Done');
                return Command::SUCCESS;
            }
            else {
                $errors = $user->getMessages();
            }
        }
        else {
            $errors = $this->app->auth()->getMessages();
        }

        $this->app->auth()->rollbackTransaction();
        $messages = Trans::trans($this->app->locale(), $errors);
        foreach($messages as $name => $list) {
            throw new \InvalidArgumentException("$name: " . $list[0]);
        }
    }
}
