<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ClearCache extends Command
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        $this->app = $app;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Clear cache (Symfony\Component\Cache\Adapter\FilesystemAdapter).')
            ->addOption('force', null, InputArgument::REQUIRED, 'Remove all cache.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $this->app->config['__DIR__'] . $this->app->config['cache']['path'];
        $d = dir($path);
        while($entry = $d->read()) {
            $file = $path . DIRECTORY_SEPARATOR . $entry;
            if($entry != '.' && $entry != '..' && $entry != '.gitkeep' && is_dir($file)) {
                if(!($t = $this->clearDir($file, $input->getOption('force')))) {
                    rmdir($file);
                }
            }
        }

        $output->writeln('Done');
        return Command::SUCCESS;
    }

    protected function clearDir($path, $force)
    {
        $inner = 0;
        $d = dir($path);
        while($entry = $d->read()) {
            $file = $path . DIRECTORY_SEPARATOR . $entry;
            if($entry != '.' && $entry != '..') {
                if(is_dir($file)) {
                    if(!($t = $this->clearDir($file, $force))) {
                        rmdir($file);
                    } else {
                        $inner++;
                    }
                }
                else {
                    // file
                    if($t = $this->checkDeleteFile($file, $force)) {
                        unlink($file);
                    } else {
                        $inner++;
                    }
                }
            }
        }
        return $inner;
    }

    protected function checkDeleteFile($file, $force)
    {
        if($force) return true;

        $handle = fopen($file, 'r');
        $time = (int)fgets($handle, 20);
        fclose($handle);

        if($time < time()) return true;
        else return false;
    }
}
