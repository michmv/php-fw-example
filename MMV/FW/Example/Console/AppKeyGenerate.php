<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Encryption\Encrypter;

class AppKeyGenerate extends Command
{
    public array $config = [];

    public function __construct(string $name, array &$config)
    {
        $this->config = $config;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate APP_KEY for this application and write to .env file')

            ->addArgument('file', InputArgument::REQUIRED, 'Path to .env file')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        $res = [];
        $flag = false;

        // check name file
        if(!preg_match('/(^\.env$|^\.env\..+$)/', pathinfo($file, \PATHINFO_BASENAME))) {
            throw new \Exception('Invalid name file. The file must have a name .env or .env.*');
        }

        // read file
        $handle = fopen($file, 'r');
        while (($buffer = fgets($handle)) !== false) {
            if(preg_match('/^APP_KEY=.*$/', $buffer)) {
                $res[] = 'APP_KEY='.$this->generateKey().\PHP_EOL;
                $flag = true;
            }
            else {
                $res[] = $buffer;
            }
        }
        fclose($handle);

        if(!$flag) {
            $res[] = 'APP_KEY='.$this->generateKey().\PHP_EOL;
        }

        // write
        file_put_contents($file, implode('', $res));

        $output->writeln('Done');
        return Command::SUCCESS;
    }

    protected function generateKey():string
    {
        return 'base64:'.
            base64_encode(Encrypter::generateKey($this->config['security']['cipher']));
    }
}
