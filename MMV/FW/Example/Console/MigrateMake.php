<?php

namespace MMV\FW\Example\Console;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;

class MigrateMake extends Command
{
    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        parent::__construct($name);
        $this->app = $app;
    }

    /**
     * @var mixed
     */
    protected $app;

    protected function configure()
    {
        $this
            ->setDescription('Create a new migration file')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the migration')
            ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filesystem = new Filesystem;

        $name = $input->getArgument('name');

        if(!$name) throw new \InvalidArgumentException('Name is empty!');

        $class = implode('', array_map('ucwords', explode(' ', str_replace(['-', '_'], ' ', $name))));

        $content = $filesystem->get(__DIR__.'/../../../../resources/migration.create.stub');
        $content = str_replace('{{ Class name }}', $class, $content);

        $path = $this->app->config['__DIR__'] .
            $this->app->config['database']['migration']['pathMigrations'] .
            '/' . date('Y_m_d_His_') . $name . '.php'
            ;

        $filesystem->put($path, $content);

        $output->writeln('<info>Writed new migration:</info> '.$path);

        $output->writeln('Done');
        return Command::SUCCESS;
    }
}
