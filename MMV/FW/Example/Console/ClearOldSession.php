<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ClearOldSession extends Command
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        $this->app = $app;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete rotten sessions.')
            ->addOption('force', null, InputArgument::REQUIRED, 'Remove all session of all users.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if($input->getOption('force')) {
            $this->app->session()->deleteAllSessionsOfAllUsers();
        }
        else {
            $this->app->session()->deleteRottenSession();
        }

        $output->writeln('Done');
        return Command::SUCCESS;
    }
}
