<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ClearDirectory extends Command
{
    /**
     * @var \App\Example
     */
    protected $app;

    protected $description;

    protected $directory;

    /**
     * @param string $name
     * @param mixed $app
     * @param string $description
     * @param string $directory
     */
    public function __construct(string $name, $app, string $description, string $directory)
    {
        $this->app = $app;
        $this->description = $description;
        $this->directory = $directory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription($this->description)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $this->app->config['__DIR__'] . $this->directory;
        $this->clearDirectory($path);

        $output->writeln('Done');
        return Command::SUCCESS;
    }

    protected function clearDirectory($path)
    {
        $d = dir($path);
        while($entry = $d->read()) {
            $file = $path . DIRECTORY_SEPARATOR . $entry;
            if($entry != '.' && $entry != '..' && $entry != '.gitkeep') {
                if(is_dir($file)) {
                    $this->clearDirectory($file);
                    rmdir($file);
                }
                else {
                    unlink($file);
                }
            }
        }
    }
}
