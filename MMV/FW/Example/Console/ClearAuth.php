<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ClearAuth extends Command
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        $this->app = $app;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Clear old records database for Auth module.')
            ->addOption('force', null, InputArgument::REQUIRED, 'Clear all users without email confirm.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $auth = $this->app->auth();

        if($input->getOption('force')) {
            // with force
            $offset = 0;
            $time = time() * 2;
            var_dump('with force');
        }
        else {
            // without force
            $offset = 60 * 60 * 24;
            $time = time();
            var_dump('without force');
        }

        $auth->clearNoConfirmUser($time, $offset);
        $auth->clearEmailConfirm($time);
        $auth->clearFailedSignin($time);
        $auth->clearPasswordReset($time);

        $output->writeln('Done');
        return Command::SUCCESS;
    }
}
