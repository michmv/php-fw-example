<?php

namespace MMV\FW\Example\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use MMV\FW\Example\Db\Migrator;
use Illuminate\Filesystem\Filesystem;

abstract class MigrateAbstract extends Command
{
    /**
     * @param string $name
     * @param mixed $app
     */
    public function __construct(string $name, $app)
    {
        $this->app = $app;
        parent::__construct($name);
    }

    /**
     * @var mixed
     */
    protected $app;

    protected function getMigrator(OutputInterface $output)
    {
        $repository = new DatabaseMigrationRepository(
            $this->app->db()->getDatabaseManager(),
            $this->app->config['database']['migration']['tableMigrates']
        );

        if(!$repository->repositoryExists())
            $repository->createRepository();

        $migrator = new Migrator(
            $repository,
            $this->app->db()->getDatabaseManager(),
            new Filesystem()
            );
        $migrator->setOutput($output);

        return $migrator;
    }
}
