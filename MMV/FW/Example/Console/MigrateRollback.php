<?php

namespace MMV\FW\Example\Console;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use MMV\FW\Example\Console\MigrateAbstract;

class MigrateRollback extends MigrateAbstract
{
    protected function configure()
    {
        $this
            ->setDescription('Rollback the last database migration')
            ->addOption('step', null, InputArgument::OPTIONAL, 'The number of migrations to be reverted', 1)
            ->addOption('pretend', null, InputArgument::REQUIRED, 'Dump the SQL queries that would be run')
            ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filesystem = new Filesystem;
        $files = $filesystem->files(
            $this->app->config['__DIR__'].$this->app->config['database']['migration']['pathMigrations']);

        if($files) {
            $migrator = $this->getMigrator($output);
            $migrator->rollback($files, [
                'step' => $input->getOption('step'),
                'pretend' => (bool)$input->getOption('pretend'),
            ]);
        }

        $output->writeln('Done');
        return Command::SUCCESS;
    }
}
