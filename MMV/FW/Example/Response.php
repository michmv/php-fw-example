<?php

namespace MMV\FW\Example;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse
{
    /**
     * @throws \InvalidArgumentException When the HTTP status code is not valid
     */
    public function __construct(?string $content = '', int $status = 200, array $headers = [])
    {
        parent::__construct($content, $status, $headers);

        // add status code
        static::$statusTexts[419] = 'Page Expired';
    }

    public function __toString()
    {
        $this->sendHeaders();
        return ($this->getJson()) ?
            json_encode($this->content, \JSON_PRETTY_PRINT) :
            $this->content;
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function setJson($data, bool $status=true)
    {
        $this->isJson = $status;
        $this->content = $data;
        $this->headers->set('Content-Type', 'application/json');

        return $this;
    }

    /**
     * @return boolean
     */
    public function getJson(): bool
    {
        return $this->isJson;
    }

    /**
     * Returns the target URL.
     *
     * @return string target URL
     */
    public function getTargetUrl()
    {
        return $this->targetUrl;
    }

    /**
     * Sets the redirect target of this response.
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setTargetUrl(string $url)
    {
        if ('' === $url) {
            throw new \InvalidArgumentException('Cannot redirect to an empty URL.');
        }

        $this->targetUrl = $url;

        $this->setContent(
            sprintf('<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="0;url=\'%1$s\'" />

        <title>Redirecting to %1$s</title>
    </head>
    <body>
        Redirecting to <a href="%1$s">%1$s</a>.
    </body>
</html>', htmlspecialchars($url, \ENT_QUOTES, 'UTF-8')));

        $this->headers->set('Location', $url);

        return $this;
    }

    /**
     * @var mixed
     */
    protected $content;

    /**
     * @var string
     */
    protected $targetUrl;

    /**
     * @var bool
     */
    protected $isJson = false;
}
