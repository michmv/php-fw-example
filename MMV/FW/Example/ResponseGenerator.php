<?php

namespace MMV\FW\Example;

use MMV\FW\Example\View;
use MMV\FW\Example\Template;
use MMV\FW\Example\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use MMV\Auth\Low\Session;
use MMV\FW\Example\UrlGenerator;
use MMV\FW\Example\Redirect;

class ResponseGenerator
{
    public Template $template;

    public ResponseHeaderBag $headers;

    public Response $response;

    public Session $session;

    public UrlGenerator $urlGenerator;

    public function __construct(Request $request, Template $template, UrlGenerator $urlGenerator, Session $session)
    {
        $this->template = $template;
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;

        $this->response = new Response;
        $this->response->prepare($request);
        $this->headers = $this->response->headers;
    }

    public function view(string $path, array $parameters=[])
    {
        return $this->response->setContent(new View($this->template, $path, $parameters));
    }

    public function redirect(string $url='', int $code=302)
    {
        $redirect = new Redirect($this->session, $this->response, $this->urlGenerator);
        $redirect->url = $url;
        $redirect->code = $code;
        return $redirect;
    }

    public function abort(int $code, string $message='')
    {
        $this->response->setStatusCode($code);
        $this->response->setContent(new View($this->template, '@fw-example/abort.html.twig', [
            'error' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : 'Unknown status',
            'code' => $code,
            'message' => $message,
        ]));
        return $this->response;
    }

    public function json($data)
    {
        return $this->response->setJson($data);
    }
}