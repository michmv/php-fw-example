<?php

namespace MMV\FW\Example\Widgets\Utility;

trait MultiSelectTrait
{
    public function checkSelect($now, &$value)
    {
        if(is_string($value)) {
            return (string)$now === (string)$value;
        }
        else if(is_array($value)) {
            return in_array($now, $value, true);
        }
        return false;
    }

}
