<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;

class Textarea extends Input
{
    public $view = '@fw-example/widgets/textarea.html.twig';

    public $rows = 8;
}
