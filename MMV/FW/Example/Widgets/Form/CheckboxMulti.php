<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;
use MMV\FW\Example\Widgets\Utility\MultiSelectTrait;

class CheckboxMulti extends Input
{
    use MultiSelectTrait;

    public $view = '@fw-example/widgets/checkbox_multi.html.twig';

    public $type = 'checkbox';

    public $options = [];

    public $inline = false;

    protected function getValue()
    {
        if(is_object($this->value)) {
            $property = $this->name;
            $res = ($this->value->$property ?? null);
        }
        else if(is_array($this->value)) {
            $res = ($this->value[$this->name] ?? null);
        }
        else {
            $res = $this->value;
        }

        return $res;
    }
}
