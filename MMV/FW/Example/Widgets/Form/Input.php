<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widget;
use MMV\FW\Example\Widgets\Utility\ErrorsTrait;

class Input extends Widget
{
    use ErrorsTrait;

    public $name = 'field';

    public $alias = '';

    public $label = '';

    public $type = 'text';

    public $required = false;

    public $autofocus = false;

    public $disabled = false;

    public $placeholder = '';

    public $help = '';

    /**
     * @var mixed
     */
    public $value;

    public $view = '@fw-example/widgets/input.html.twig';

    public function __toString()
    {
        return $this->app->template()->render($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
            'value' => $this->getValue(),
        ]);
    }

    public function name()
    {
        return $this->alias ? $this->alias : $this->name;
    }

    protected function getValue()
    {
        if(is_object($this->value)) {
            $property = $this->name;
            $res = ($this->value->$property ?? null);
        }
        else if(is_array($this->value)) {
            $res = ($this->value[$this->name] ?? null);
        }
        else {
            $res = $this->value;
        }

        if(is_array($res)) $res = '[]';

        return (string)$res;
    }
}
