<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;

class Checkbox extends Input
{
    public $view = '@fw-example/widgets/checkbox.html.twig';
    
    /**
     * @var string
     */
    public $defaultOff = '0';

    /**
     * @var string
     */
    public $defaultOn = '1';

    public $type = 'checkbox';
}
