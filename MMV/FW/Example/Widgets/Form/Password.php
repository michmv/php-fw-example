<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;

class Password extends Input
{
    public function __toString()
    {
        $this->type = 'password';

        return $this->app->template()->render($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
            'value' => '',
        ]);
    }
}
