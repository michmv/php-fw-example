<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;
use MMV\FW\Example\Widgets\Utility\MultiSelectTrait;

class Select extends Input
{
    use MultiSelectTrait;

    public $view = '@fw-example/widgets/select.html.twig';

    public $defaultValue = '';

    public $defaultLabel = '-';

    public $options = [];
}
