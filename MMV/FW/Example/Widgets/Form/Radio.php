<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\Input;
use MMV\FW\Example\Widgets\Utility\MultiSelectTrait;

class Radio extends Input
{
    use MultiSelectTrait;

    public $options = [];

    public $default = '';

    public $view = '@fw-example/widgets/radio.html.twig';

    public $inline = false;

    public $type = 'radio';
}
