<?php

namespace MMV\FW\Example\Widgets\Form;

use MMV\FW\Example\Widgets\Form\CheckboxMulti;

class SelectMulti extends CheckboxMulti
{
    public $view = '@fw-example/widgets/select_multi.html.twig';

    public $size = 6;
}
