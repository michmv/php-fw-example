<?php

namespace MMV\FW\Example\Widgets;

use MMV\FW\Example\Widget;
use MMV\FW\Example\Widgets\TableGrid\StrategyInterface;
use MMV\FW\Example\Utility;

class TableGrid extends Widget
{
    /**
     * Implements \MMV\FW\Example\Widgets\TableGrid\StrategyInterface
     * 
     * @var string|object 
     */
    public $strategy;

    /**
     * Id for html
     * 
     * @var string
     */
    public $id = 'table-grid';

    /**
     * Limit rows on one page
     *
     * @var integer
     */
    public $limit = 0;

    /**
     * Collection
     *
     * @var mixed
     */
    public $collection = [];

    /**
     * If is value string to convert object. If array to check key `class`.
     *
     * @var array String, Array or Object
     */
    public $columns = [];

    /**
     * If is value string to convert object. If array to check key `class`.
     *
     * @var array String, Array or Object
     */
    public $filters = [];

    /**
     * @var string
     */
    public $defaultColumn = \MMV\FW\Example\Widgets\TableGrid\Column::class;

    /**
     * @var string
     */
    public $defautlFilter = \MMV\FW\Example\Widgets\TableGrid\Filter::class;

    /**
     * Params for href
     *
     * @var array
     */
    public $params = [];

    /**
     * Name template for render
     *
     * @var string
     */
    public $view = '@fw-example/widgets/table_grid.html.twig';

    /**
     * Default value for view
     * Keys:
     *   gridFilter => 'col-sm-4';
     *   tableResponsive => 'table-responsive';
     *
     * @var array
     */
    public $viewParams = [];

    /**
     * More resources for table grid
     *
     * @var array
     */
    public $resources = [];

    public function __toString()
    {
        $strategy = $this->getStrategy();

        $columns = Utility::toObjectList($this->columns, $this->defaultColumn);
        $filters = Utility::toObjectList($this->filters, $this->defautlFilter);

        $request = $this->app->request();
        $import = $request->query->all();
        $params = $this->params;

        list($params, $filters) = $this->applyFilter($strategy, $filters, $params, $import);
        $params = $this->applySort($strategy, $columns, $params, $import);
        list($total, $select, $params) = $this->applySlice($strategy, $params, $import);

        $collection = $strategy->get($this->collection);

        return $this->app->template()->render($this->view, [
            'it' => $this,
            'collection' => $collection,
            'columns' => $columns,
            'filters' => $filters,
            'params' => $this->cleanEmptyParams($params),
            'paginator' => ['total'=>$total, 'select'=>$select],
            'resources' => $this->compileResources($this->compileResources($this->resources, $filters), $columns),
            'active' => $this->active($filters),
        ]);
    }

    protected function applyFilter($strategy, $filters, $params, $import)
    {
        foreach($filters as $index => $filter) {
            if(array_key_exists('f'.$index, $import)) {
                $params['f'.$index] = trim($import['f'.$index]);
                [$this->collection, $filter] = $strategy->filter($this->collection, $filter, $params['f'.$index]);
                $filters[$index] = $filter;
            }
        }
        return [$params, $filters];
    }

    protected function applySort($strategy, $columns, $params, $import)
    {
        if(array_key_exists('s', $import)) {
            if(array_key_exists($import['s'], $columns) && ($columns[$import['s']])->sort) {
                $params['s'] = (int)$import['s'];
                $t = (int)($import['t'] ?? 0);
                if(in_array($t, [0, 1])) {
                    $params['t'] = $t;
                }
                else {
                    $params['t'] = 0;
                }
                $this->collection = $strategy->sort($this->collection, $columns[$params['s']], $params['t']);
            }
        }
        return $params;
    }

    protected function applySlice($strategy, $params, $import)
    {
        if($this->limit == 0) {
            $total = 1;
            $select = 1;
        }
        else {
            $total = ceil($strategy->count($this->collection) / $this->limit);

            $select = (int)($import['p'] ?? 1);
            if($select < 1 || $select > $total) $select = 1;

            if($select > 1) $params['p'] = $select;

            $this->collection = $strategy->slice($this->collection, $select, $this->limit);
        }
        return [$total, $select, $params];
    }

    protected function getStrategy()
    {
        if(is_string($this->strategy)) {
            $class = str_replace('.', '\\', $this->strategy);
            $res = new $class;
        } else {
            $res = $this->strategy;
        }

        if($res instanceof StrategyInterface) {
            return $res;
        } else {
            throw new \RuntimeException('Strategy for table grid must implements StrategyInterface');
        }
    }

    protected function cleanEmptyParams($params)
    {
        foreach($params as $key => $val) {
            if($val === '') unset($params[$key]);
        }
        return $params;
    }
    
    protected function compileResources($source, $add)
    {
        foreach($add as $it) {
            $res = $it->resources($this->app->helper(), $this->id, $this->viewParams);
            if($res) $source = array_merge($source, $res);
        }
        return $source;
    }

    protected function active($filters)
    {
        foreach($filters as $filter) {
            if($filter->active) return true;
        }
        return false;
    }
}
