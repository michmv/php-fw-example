<?php

namespace MMV\FW\Example\Widgets;

use MMV\FW\Example\Widget;

class Breadcrumb extends Widget
{
    /**
     * @var string|string[]
     */
    public $path = [];

    public function __toString()
    {
        $res = [];

        $n = 0;
        $last = count($this->path) - 1;
        foreach($this->path as $point) {
            if($n != $last) {
                $res[] = $this->htmlLink($point[1], $point[0]);
            } else {
                $res[] = $this->htmlLast(is_array($point) ? $point[0] : $point);
            }
            $n++;
        }

        return $this->htmlOl($res);
    }

    /**
     * @param string[] $inner
     * @return void
     */
    protected function htmlOl($inner)
    {
        return ($inner) ?
            '<ol class="breadcrumb">'.implode('', $inner).'</ol>' :
            '';
    }

    /**
     * @param string $url
     * @param string $title
     * @return string
     */
    protected function htmlLink($url, $title)
    {
        return '<li class="breadcrumb-item"><a href="'.$this->app->helper()->escape($url).'">'.$this->app->helper()->escape($title).'</a></li>';
    }

    /**
     * @param string $title
     * @return string
     */
    protected function htmlLast($title)
    {
        return '<li class="breadcrumb-item active">'.$this->app->helper()->escape($title).'</li>';
    }
}
