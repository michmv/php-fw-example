<?php

namespace MMV\FW\Example\Widgets\TableGrid;

class Filter
{
    /**
     * Name field
     *
     * @var string
     */
    public $field = '';

    /**
     * Title for show
     *
     * @var string
     */
    public $title = '';

    /**
     * Set True if  it filter apply for collection, False if filter not have value.
     *
     * @var boolean
     */
    public $active = false;

    public function __construct($field='')
    {
        $this->field = $field;
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $index
     * @param array $params
     * @param int $colSmNum
     * @param array $viewParams
     * @return string
     */
    public function htmlCol($helper, $index, $params, $viewParams)
    {
        $gridFilter = $viewParams['gridFilter'] ?? 'col-sm-4';

        return '<div class="input-group input-group-sm mb-3 '.$gridFilter.'">'.
                    '<div class="input-group-prepend">'.
                        '<span class="input-group-text">'.$this->getTitle($helper).'</span>'.
                    '</div>'.
                    '<input type="text" class="form-control" name="f'.$index.'" value="'.$helper->escape($params['f'.$index] ?? '').'">'.
                '</div>';
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $id
     * @param array $viewParams
     * @return array
     */
    public function resources($helper, $id, $viewParams)
    {
        return [];
    }

    /**
     * Custome filter
     * 
     * @param mixed $collection
     * @param string $value
     * @return array [mixed $collection, mixed $filter]
     */
    // public function filter($collection, $value)
    // {
    //     // ..
    //     return [$collection, $this];
    // }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @return string
     */
    protected function getTitle($helper)
    {
        if($this->title) {
            $title = $this->title;
        }
        else {
            $fc = mb_strtoupper(mb_substr($this->field, 0, 1));
            $title = $fc.mb_substr($this->field, 1);
        }
        return $helper->escape($title);
    }
}
