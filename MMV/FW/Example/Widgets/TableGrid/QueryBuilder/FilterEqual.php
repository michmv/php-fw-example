<?php

namespace MMV\FW\Example\Widgets\TableGrid\QueryBuilder;

use MMV\FW\Example\Widgets\TableGrid\Filter;

class FilterEqual extends Filter
{

    /**
     * Custome filter
     *
     * @param mixed $collection
     * @param string $value
     * @return array [mixed $collection, mixed $filter]
     */
    public function filter($collection, $value)
    {
        if($value !== '') {
            $this->active = true;
            $field = $this->field;

            $collection->where($field, '=', $value);
        }

        return [$collection, $this];
    }
}
