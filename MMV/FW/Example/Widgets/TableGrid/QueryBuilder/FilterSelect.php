<?php

namespace MMV\FW\Example\Widgets\TableGrid\QueryBuilder;

use MMV\FW\Example\Widgets\TableGrid\QueryBuilder\FilterEqual;

class FilterSelect extends FilterEqual
{
    /**
     * @var string
     */
    public $disableSelect = '-';

    /**
     * List
     *
     * @var string[]
     */
    public $options = [];

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $index
     * @param array $params
     * @param int $colSmNum
     * @param array $viewParams
     * @return string
     */
    public function htmlCol($helper, $index, $params, $viewParams)
    {
        $options = $this->disableSelect ?
            ['' => $this->disableSelect] + $this->options :
            $this->options;

        $value = $params['f'.$index] ?? '';

        $res = [];
        foreach($options as $key => $val) {
            $selected = ((string)$key === (string)$value) ? ' selected' : '';
            $res[] = '<option'.$selected.' value="'.$helper->escape($key).'">'.$helper->escape($val).'</option>';
        }

        $gridFilter = $viewParams['gridFilter'] ?? 'col-sm-4';

        return
            '<div class="input-group input-group-sm mb-3 '.$gridFilter.'">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="filterSelect'.$index.'">'.$this->getTitle($helper).'</label>
                </div>
                <select name="f'.$index.'" class="custom-select filter-select" id="filterSelect'.$index.'">'.implode('', $res).'</select>
            </div>';
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $id
     * @param array $viewParams
     * @return array
     */
    public function resources($helper, $id, $viewParams)
    {
        $res =
            '<script>
                panelAdmin.addInitFunction(function(){
                    $("#'.$id.' .filter-select").on("change", function(){
                        var form = $(this).parents("form")
                        form.submit()
                    })
                })
            </script>'
            ;
        return ['SelectFilter' => $res];
    }
}
