<?php

namespace MMV\FW\Example\Widgets\TableGrid;

use MMV\FW\Example\Utility;

class Column
{
    /**
     * Name field
     *
     * @var string
     */
    public $field = '';

    /**
     * Title for show
     *
     * @var string
     */
    public $title = '';

    /**
     * Active translate
     *
     * @var boolean
     */
    public $translate = false;

    /**
     * @var boolean
     */
    public $escape = true;

    /**
     * Active sort function for this column
     *
     * @var boolean
     */
    public $sort = false;

    public function __construct($field='')
    {
        $this->field = $field;
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $index
     * @param array $params
     * @param array $viewParams
     * @return string
     */
    public function htmlCellHead($helper, $index, $params, $viewParams)
    {
        return '<th class="n'.$index.'">'.$this->getTitle($helper, $index, $params).'</th>';
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param object $row
     * @param int $index
     * @param array $viewParams
     * @return string
     */
    public function htmlCellBody($helper, $row, $index, $viewParams)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->trans($field);
        if($this->escape) $field = $helper->escape($field);
        return '<td class="n'.$index.'">'.$field.'</td>';
    }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $id
     * @param array $viewParams
     * @return array
     */
    public function resources($helper, $id, $viewParams)
    {
        return [];
    }

    /**
     * Custom sort
     *
     * @param mixed $collection
     * @param int $type 0 is ASC, 1 is DESC
     * @return mixed
     */
    // public function sort($collection, $type)
    // {
    //     //
    //     return $collection;
    // }

    /**
     * @param \MMV\FW\Example\Helper $helper
     * @param int $index
     * @param array $params
     * @return string
     */
    protected function getTitle($helper, $index, $params)
    {
        if($this->title) {
            $title = $this->title;
        }
        else {
            $fc = mb_strtoupper(mb_substr($this->field, 0, 1));
            $title = $fc.mb_substr($this->field, 1);
        }
        $title = $helper->escape($title);

        // no sort
        if(!$this->sort) return $title;

        if(array_key_exists('s', $params) && $index == $params['s']) {
            if($params['t'] == 1) $icon = 'sort-up';
            else $icon = 'sort-down';
            $params['t'] = ($params['t'] == 0) ? 1 : 0;
        }
        else {
            // default
            $icon = 'sort';
            $params['s'] = $index;
            $params['t'] = 0;
        }

        $url = Utility::urlPlusParams($helper->url->current(), $params);
        return '<a class="text-white" href="'.$url.'">'.$title.' <i class="fas fa-'.$icon.'"></i></a>';
    }
}
