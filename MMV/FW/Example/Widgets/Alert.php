<?php

namespace MMV\FW\Example\Widgets;

use MMV\FW\Example\Widget;

class Alert extends Widget
{
    /**
     * primary, secondary, success, danger, warning, info, light, dark
     * 
     * @var string
     */
    public $type = "dark";

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string|array
     */
    public $messages;

    public $view = '@fw-example/widgets/alert.html.twig';

    public function __toString()
    {
        if(!$this->messages) return '';

        if(is_array($this->messages)) {
            $messages = $this->messages;
        } else {
            $messages = [(string)$this->messages];
        }

        return $this->app->template()->render($this->view, [
            'it' => $this,
            'messages' => $messages,
        ]);
    }
}
