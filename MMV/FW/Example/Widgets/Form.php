<?php

namespace MMV\FW\Example\Widgets;

use MMV\FW\Example\Widget;
use MMV\FW\Example\Widgets\Utility\ErrorsTrait;

class Form extends Widget
{
    use ErrorsTrait;

    public $id = '';

    public $action = '';

    public $method = 'post';

    public $enctype = 'multipart/form-data';

    public $name = 'form';

    public $view = '@fw-example/widgets/form.html.twig';

    /**
     * Simple spam protect
     */
    public $protect = false;

    public function __toString()
    {
        if(!$this->action) $this->action = $this->app->helper()->url->current();

        return $this->app->template()->render($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
            ]);
    }
}
