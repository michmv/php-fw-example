<?php

namespace MMV\FW\Example\Widgets\Backend;

use MMV\FW\Example\Backend\MenuItemLeft;
use MMV\FW\Example\Widgets\Backend\MenuTop;

class MenuLeft extends MenuTop
{
    public $select = '';

    public function __toString()
    {
        $list = $this->app->eventDispatcher()->buildBackendLeftMenu($this->buildMenu());
        $list = array_merge($list['first'], $list['last']);
        /** @var MenuItemLeft[] $list */

        return $this->htmlMenu($list, 0);
    }

    protected function buildMenu()
    {
        return [
            'first' =>
                [ MenuItemLeft::item('backend@menu.open-site', 'home', '', true) ],
            'last' =>
                [ MenuItemLeft::separator()
                , MenuItemLeft::list('backend@menu.management', '', [
                        MenuItemLeft::item('backend@role.title', 'backend.user.role.list', 'administration.users'),
                        MenuItemLeft::item('backend@user.title', 'backend.user.list', 'administration.users'),
                    ]),
                ]
        ];
    }

    /**
     * @param MenuItemLeft[] $list
     * @param int $depth
     * @return string
     */
    protected function htmlMenu($list, $depth)
    {
        $res = [];
        foreach($list as $item) {
            if($this->access($item->resources)) {
                if($item->separator) {
                    $res[] = 'separator';
                }
                else {
                    if($item->child) {
                        // submenu
                        $sub = $this->htmlMenu($item->child, $depth+1);
                        if($sub) {
                            $title = $this->app->helper()->trans($item->title);
                            $res[] = '<li class="pal-sub"><span class="pal-text">'.$title.'</span>'.$sub.'</li>';
                        }
                    }
                    else {
                        // menu
                        $link = $this->urlGenerate($item->link);
                        $title = $this->app->helper()->trans($item->title);
                        $select = $item->title == $this->select && $this->select != '';
                        $res[] = '<li>'.$this->genLinkEx($title, $link, $item->newTab, $select).'</li>';
                    }
                }
            }
        }
        $res = $this->separatorToTag($this->filterSeparator($res));
        return $res ? '<ul'.($depth == 0 ? ' class="pal-menu"' : '').'>'
            .implode('', $res).'</ul>' : '';
    }

    protected function separatorToTag($items)
    {
        foreach($items as $key => $val) {
            if($val == 'separator')
                $items[$key] = '<li><span class="pal-separation pal-text"><span></span></span></li>';
        }
        return $items;
    }

    protected function genLinkEx($title, $link, $newTab, $select)
    {
        $icon = $target = '';
        if($newTab) {
            $icon = ' <i class="fas fa-external-link-alt"></i>';
            $target = ' target="_blank"';
        }
        return '<a'.$target.' class="pal-text'.($select ? ' pal-select' : '').'" href="'.$link.'" title="'.$title.'">'.$title.$icon.'</a>';
    }
}
