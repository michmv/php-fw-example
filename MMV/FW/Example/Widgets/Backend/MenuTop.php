<?php

namespace MMV\FW\Example\Widgets\Backend;

use MMV\FW\Example\Widget;
use MMV\FW\Example\Backend\MenuItemTop;

class MenuTop extends Widget
{
    public function __toString()
    {
        $list = $this->app->eventDispatcher()->buildBackendTopMenu($this->buildMenu());
        $list = array_merge($list['first'], $list['last']);
        /** @var MenuItemTop[] $list */

        $res = [];
        foreach($list as $item) {
            if($this->access($item->resources)) {
                if($item->separator) {
                    $res[] = 'separator';
                }
                else {
                    $link = $this->urlGenerate($item->link);
                    $title = $this->app->helper()->trans($item->title);
                    $res[] = '<li>'.$this->genLink($title, $link, $item->newTab).'</li>';
                }
            }
        }
        $res = $this->separatorToTag($this->filterSeparator($res));
        return $this->getButton() . ($res ? '<ul class="scroll">'.implode('', $res).'</ul>' : '');
    }

    protected function buildMenu()
    {
        return [
            'first' =>
                [ MenuItemTop::item('backend@menu.change-password', 'auth.change.password') ],
            'last' =>
                [ MenuItemTop::item('backend@menu.signout', 'auth.signout')
                , MenuItemTop::separator()
                , MenuItemTop::item('backend@menu.signout-all', 'auth.signout.all')
                ]
        ];
    }

    protected function getButton()
    {
        $userName = $this->app->helper()->escape($this->app->auth()->user()->login);
        return '<div class="pat-menu-title" title="'.$userName.'">'.$userName.'</div>';
    }

    protected function genLink($title, $link, $newTab)
    {
        $icon = $target = '';
        if($newTab) {
            $icon = ' <i class="fas fa-external-link-alt"></i>';
            $target = ' target="_blank"';
        }
        return '<a'.$target.' href="'.$link.'" title="'.$title.'">'.$title.$icon.'</a>';
    }

    protected function separatorToTag($items)
    {
        foreach($items as $key => $val) {
            if($val == 'separator')
                $items[$key] = '<li><span class="pat-separation"></span></li>';
        }
        return $items;
    }

    protected function filterSeparator($items)
    {
        $res = [];

        // remove dublicate
        $separator = false;
        foreach($items as $item) {
            if($item == 'separator') {
                if(!$separator) {
                    $res[] = $item;
                    $separator = true;
                }
            }
            else {
                $res[] = $item;
                $separator = false;
            }
        }

        if(!$res) return [];

        // remove first and last separator
        if($res[count($res)-1] == 'separator') unset($res[count($res)-1]);
        if(isset($res[0]) && $res[0] == 'separator') unset($res[0]);

        return $res;
    }

    protected function access($resources)
    {
        if($resources == '' || $resources == []) return true;

        if(is_string($resources)) $resources = [$resources];
        foreach($resources as $resource) {
            if(!$this->app->auth()->check($resource)) return false;
        }
        return true;
    }

    protected function urlGenerate($link)
    {
        if(is_string($link)) $link = ['route', $link];

        return call_user_func_array([$this->app->helper()->url, $link[0]], array_slice($link, 1));
    }
}
