<?php

namespace MMV\FW\Example;

use MMV\FW\Core\RoutesInterface;
use Symfony\Component\Routing\RouteCollection;
use MMV\FW\Core\Execute;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\RequestContext;
use MMV\FW\Example\RouteConfigurator;
use Symfony\Component\Routing\Route;
use MMV\FW\Core\Controller;
use MMV\FW\Core\Callback;
use Symfony\Component\HttpFoundation\Request;

class Routes implements RoutesInterface
{
    public RequestContext $context;

    protected RouteCollection $collection;

    protected Request $request;

    public function __construct(RouteCollection $collection, Request $request)
    {
        $this->collection = $collection;
        $this->request = $request;

        $this->context = new RequestContext();
        $this->context->fromRequest($request);
    }

    public function add(string $name, string $path): RouteConfigurator
    {
        $route = new Route($path);
        $this->collection->add($name, $route);
        return new RouteConfigurator($route);
    }

    public function getCollection(): RouteCollection
    {
        return $this->collection;
    }

    public function getExecute(): Execute
    {
        $matcher = new UrlMatcher($this->collection, $this->context);

        try {
            $parameters = $matcher->match($this->context->getPathInfo());
            $parametersForExecute = [];

            // filter parameters for Execute class and
            // add parameters to Request
            foreach($parameters as $key => $val) {
                if($key == '_execute' || $key == '_type') continue; // filter

                if($key[0] != '_') $parametersForExecute[$key] = $val;

                $this->request->attributes->set($key, $val); // add parameters from Route to Request
            }

            // add locale ot Request
            if(array_key_exists('_locale', $parameters))
                $this->request->setLocale($parameters['_locale']);

            if(!array_key_exists('_execute', $parameters))
                throw new \RuntimeException('It is impossible to understand how to run the application');

            if($parameters['_type'] == RouteConfigurator::CONTROLLER) {
                $execute = new Controller(
                    $parameters['_execute'][0],
                    $parameters['_execute'][1],
                    $parametersForExecute
                );
            }
            else if($parameters['_type'] == RouteConfigurator::CALLBACK) {
                $execute = new Callback(
                    $parameters['_execute'],
                    $parametersForExecute
                );
            }
            else {
                throw new \RuntimeException('Can\'t execute application');
            }

            return $execute;
        }
        catch (ResourceNotFoundException $e) {
            return $this->abort404();
        }
        catch (MethodNotAllowedException $e) {
            return $this->abort404();
        }
    }

    protected function abort404()
    {
        return new Callback(function($app) { return $app->response()->abort(404); });
    }
}
