<?php

namespace MMV\FW\Example;

use Twig\Environment;
use Twig\Loader;
use MMV\FW\Example\Monolog\Logger;

class Template
{
    public Environment $twig;

    protected ?Logger $logger;

    protected bool $onLogger = false;

    public function __construct(array &$config, Logger $logger=null)
    {
        $this->logger = $logger;
        $this->onLogger = (bool)$config['debug'] && (bool)$config['template']['logger'];

        $loader = new Loader\FilesystemLoader();
        foreach($config['template']['directories'] as $key => $val) {
            $loader->addPath($config['__DIR__'].$val, $key);
        }

        $twig = new Environment($loader, [
            'cache' => $config['__DIR__'].$config['template']['cache'],
            'debug' => (bool)$config['debug'],
        ]);

        $this->twig = $twig;
    }

    public function render(string $path, array $parameters=[]): string
    {
        if($this->onLogger && $this->logger) {
            $this->logger->debug('TEMPLATE', ['path'=>$path, 'parameters'=>$this->getParametersForOut($parameters, 1)]);
        }
        return $this->twig->render($path, $parameters);
    }

    protected function getParametersForOut($array, $level)
    {
        $res = [];
        foreach($array as $key => $val) {
            $res[$key] = $this->getTypeOfValue($val, $level+1);
        }
        return $res;
    }

    protected function getTypeOfValue($val, $level)
    {
        if(is_array($val)) {
            if($level > 2) {
                $res = $val ? 'array [...]' : [];
            }
            else {
                $res = $this->getParametersForOut($val, $level);
            }
        }
        else if(is_object($val)) {
            $res = 'object '.get_class($val);
        }
        else {
            $res = (string)$val;
        }

        return $res;
    }
}
