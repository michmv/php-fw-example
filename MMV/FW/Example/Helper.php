<?php

namespace MMV\FW\Example;

use MMV\FW\Example\UrlGenerator;
use MMV\FW\Example\HeadGenerator;

class Helper
{
    public ?UrlGenerator $url = null;

    public ?HeadGenerator $head = null;

    protected $cache = [
        'selectBackendLeftMenu' => '',
    ];

    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param \App\Example $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        $this->url = new UrlGenerator(
            $app->request(),
            $app->routes()->getCollection(),
            $app->routes()->context
            );

        $this->head = new HeadGenerator($app->config['head'], $app->template(), $app->locale());
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function config($name, $default=null)
    {
        $path = explode('.', $name);
        $config = $this->app->config;
        foreach($path as $key) {
            if(array_key_exists($key, $config)) {
                $config =& $config[$key];
            }
            else {
                return $default;
            }
        }
        return $config;
    }

    public function getLocale()
    {
        return $this->app->request()->getLocale();
    }

    public function csrfField(): string
    {
        return $this->app->config['security']['csrf']['name'];
    }

    public function csrfValue(): string
    {
        $name = $this->csrfField();
        $token = (string)$this->app->session()->get($name, '');
        $time_life = (int)$this->app->session()->get($name.'_time_life', 0);

        if(!$token || $time_life < time()) {
            $token = $this->app->security()->randomString(32);
            $this->app->session()->set($name, $token);
        }

        $this->app->session()
            ->set($name.'_time_life', time() + $this->app->config['security']['csrf']['timeLife']);

        return $token;
    }

    public function trans($id, $parameters=[], $domain='messages')
    {
        return $this->app->locale()->trans($id, $parameters, $domain);
    }

    public function escape($string, $strategy = 'html', $charset = null, $autoescape = false)
    {
        return twig_escape_filter($this->app->template()->twig, $string, $strategy, $charset, $autoescape);
    }

    /**
     * @return bool
     */
    public function isGuest()
    {
        return $this->app->auth()->isGuest();
    }

    /**
     * @param string|string[] $resources
     * @return bool
     */
    public function access($resources)
    {
        return $this->app->auth()->check($resources);
    }

    /**
     * @param string $pattern
     * @param string $subject
     * @return bool
     */
    public function preg_match($pattern, $subject)
    {
        return preg_match($pattern, $subject);
    }

    public function selectBackendLeftMenu($val=null)
    {
        if($val === null) return $this->cache['selectBackendLeftMenu'];
        else $this->cache['selectBackendLeftMenu'] = $val;
    }

    /**
     * @param string|array $enable "jquery", "fontawesome", "highlightjs"
     */
    public function phpDebugBarRenderHead($enable=[])
    {
        return $this->app->logger()->phpDebugBarRenderHead(
            $this,
            (is_string($enable) ? [$enable] : $enable)
            );
    }

    public function phpDebugBarRenderBody()
    {
        return $this->app->logger()->phpDebugBarRenderBody();
    }
}
