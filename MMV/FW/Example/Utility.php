<?php

namespace MMV\FW\Example;

class Utility
{
    public static function configurate(object $obj, array &$parameters=[])
    {
        foreach($parameters as $key => $val) {
            if(property_exists($obj, $key)) {
                $obj->$key = $val;
            }
            else if(method_exists($obj, $method = 'set'.$key)) {
                $obj->$method($val);
            }
        }
        return $obj;
    }

    public static function urlPlusParams(string $url, array $params)
    {
        $res = [];
        foreach($params as $name => $value) {
            $res[] = $name . '=' . urlencode($value);
        }
        return $url . (($res) ? '?' . implode('&', $res) : '');
    }

    /**
     * List to object list
     *
     * @param array $list
     * @param string $defaultClass
     * @return array
     */
    public static function toObjectList($list, $defaultClass)
    {
        $res = [];
        foreach($list as $item) {
            if(is_string($item)) {
                $class = $defaultClass;
                $t = new $class($item);
            }
            else if(is_array($item)) {
                $class = $item['class'] ?? $defaultClass;
                $class = str_replace('.', '\\', $class);
                $t = new $class;
                $t = static::configurate($t, $item);
            }
            else {
                $t = $item;
            }
            $res[] = $t;
        }
        return $res;
    }
}
