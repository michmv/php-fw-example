<?php

namespace MMV\FW\Example;

use Throwable;
use Symfony\Component\ErrorHandler\ErrorRenderer\CliErrorRenderer;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;

class PhpErrorHandler
{
    /**
     * @param Throwable $exception
     * @param array $config
     * @param mixed $app
     * @return string
     */
    public static function handler(Throwable $exception, array $config, $app=null): string
    {
        ob_end_clean();

        if($app === null) {
            error_log($exception);
        } else {
            $app->logger()->error($exception->getMessage(), [$exception]);
        }

        return ($config['debug']) ?
            static::pageWithDebug($exception, $app) :
            static::pageWithoutDebug($exception, $app, $config);
    }

    /**
     * @param Throwable $exception
     * @param mixed $app
     * @param array $config
     * @return string
     */
    public static function pageWithoutDebug(Throwable $exception, $app, array &$config): string
    {
        if($app) {
            return $app->response()->abort(500);
        }
        else {
            http_response_code(500);
            return '<h1>500 | Internal Server Error</h1>';
        }
    }

    /**
     * @param Throwable $exception
     * @param mixed $app
     * @return string
     */
    public static function pageWithDebug(Throwable $exception, $app): string
    {
        $renderer = \in_array(\PHP_SAPI, ['cli', 'phpdbg'], true) ?
            new CliErrorRenderer() :
            new HtmlErrorRenderer(true, null, '#');

        $exception = $renderer->render($exception);

        http_response_code($exception->getStatusCode());

        return $exception->getAsString();
    }
}
