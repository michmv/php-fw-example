<?php

namespace MMV\FW\Example;

use MMV\FW\Example\Template;
use MMV\FW\Example\Translator;

class HeadGenerator
{
    protected string $title = '';

    protected string $keywords = '';

    protected string $description = '';

    protected string $view = '@fw-example/head.html.twig';

    protected array $sources = [];

    protected array $config = [];

    protected Template $template;

    protected Translator $translate;

    public function __construct(array &$config, Template $template, Translator $translator)
    {
        $this->config = $config;
        $this->template = $template;
        $this->translate = $translator;
    }

    public function getTitle(): string
    {
        return ($this->title) ? $this->title : $this->translate->trans($this->config['SEO']['title']);
    }

    public function setTitle(string $str)
    {
        $this->title = $str;
    }

    public function addTitle(string $str)
    {
        $now = $this->getTitle();
        $this->title = ($now) ? $str . ' | ' . $now : $str;
    }

    public function getKeywords(): string
    {
        return ($this->keywords) ? $this->keywords : $this->translate->trans($this->config['SEO']['keywords']);
    }

    public function setKeywords(string $str)
    {
        $this->keywords = $str;
    }

    public function getDescription(): string
    {
        return ($this->description) ? $this->description : $this->translate->trans($this->config['SEO']['description']);
    }

    public function setDescription(string $str)
    {
        $this->description = $str;
    }

    public function addSource(string $index, string $content)
    {
        $this->sources[$index] = $content;
    }

    public function __toString(): string
    {
        return $this->template->render($this->view, ['the'=>$this, 'sources' => $this->renderSources()]);
    }

    protected function renderSources(): string
    {
        $sources = array_merge($this->config['sources'], $this->sources);
        return implode("\n", $sources);
    }
}
