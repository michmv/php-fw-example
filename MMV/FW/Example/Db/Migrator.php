<?php

namespace MMV\FW\Example\Db;

use Illuminate\Database\Migrations\Migrator as IllMigrator;
use Illuminate\Support\Str;

class Migrator extends IllMigrator
{
    /**
     * Resolve a migration instance from a file.
     *
     * @param  string  $file
     * @return object
     */
    public function resolve($file)
    {
        $class = $this->getMigrationClass($file);

        return new $class($this->resolver->connection());
    }

    /**
     * Resolve a migration instance from a migration path.
     *
     * @param  string  $path
     * @return object
     */
    protected function resolvePath(string $path)
    {
        $class = $this->getMigrationClass($this->getMigrationName($path));

        if (class_exists($class)) {
            return new $class($this->resolver->connection());
        }

        return $this->files->getRequire($path);
    }
}
