<?php

namespace MMV\FW\Example\Db;

use Illuminate\Database\Migrations\Migration as IllMigration;
use Illuminate\Database\Schema\Builder;
use Illuminate\Database\Connection;

class Migration extends IllMigration
{
    public Connection $db;

    public function __construct(Connection $db)
    {
        // Illuminate\Database\MySqlConnection
        $this->db = $db;
    }

    public function schema(): Builder
    {
        return $this->db->getSchemaBuilder();
    }
}
