<?php

namespace MMV\FW\Example;

use MMV\FW\Example\Utility;

abstract class Widget
{
    /**
     * @var \App\Example
     */
    protected $app;

    /**
     * @param \App\Example $app
     * @param array $parameters
     */
    public function __construct($app, array $parameters)
    {
        $this->app = $app;
        Utility::configurate($this, $parameters);
    }

    public function __toString()
    {
        return get_called_class();
    }
}
