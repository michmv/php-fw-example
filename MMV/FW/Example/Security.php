<?php

namespace MMV\FW\Example;

use Ramsey\Uuid\Uuid;
use Illuminate\Encryption\Encrypter;
use Illuminate\Hashing\BcryptHasher;

class Security
{
    public ?Encrypter $crypt = null;

    public ?BcryptHasher $hash = null;

    public function __construct(array $options)
    {
        if(is_null($this->crypt)) {
            $this->crypt = new Encrypter(
                $this->parseKey($options['key']), $options['cipher']);
        }

        if(is_null($this->hash)) {
            $this->hash = new BcryptHasher($options['bcrypt']);
        }
    }

    public function randomString(int $length): string
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    public function uuid()
    {
        return Uuid::uuid4();
    }

    protected function parseKey(string $key): string
    {
        if(substr($key, 0, 7) === 'base64:') {
            return base64_decode(substr($key, 7));
        }
        return $key;
    }
}