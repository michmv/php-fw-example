<?php

namespace MMV\FW\Example\Auth;

use MMV\Auth\Low\StorageInterface;
use MMV\FW\Example\Database;
use stdClass;

class Storage implements StorageInterface
{
    public Database $database;

    protected string $table = '';

    public function __construct(Database $databse, string $table='')
    {
        $this->database = $databse;
        $this->table = $table;
    }

    public function setTable(string $name)
    {
        $this->table = $name;
    }

    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function insertRecord($data, string $table=null): int
    {
        return $this->database->table($this->table($table))
            ->insertGetId($data);
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function updateRecord($conditions, $data, string $table=null): int
    {
        $q = $this->database->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->update($data);
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function deleteRecord($conditions, string $table=null): int
    {
        $q = $this->database->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->delete();
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function countRecord($conditions, string $table=null): int
    {
        $q = $this->database->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->count();
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return object[] [ [row from table] ]
     */
    public function findRecord($conditions, string $table=null): ?array
    {
        $q = $this->database->table($this->table($table));
        $this->conditions($q, $conditions);

        $result =  $q->get();
        return $result ? $result->all() : [];
    }

    public function beginTransaction()
    {
        $this->database->connection()->beginTransaction();
    }

    public function commitTransaction()
    {
        $this->database->connection()->commit();
    }

    public function rollbackTransaction()
    {
        $this->database->connection()->rollBack();
    }

    protected function table($name)
    {
        return ($name) ? $name : $this->getTable();
    }

    protected function conditions($q, $conditions)
    {
        foreach($conditions as $i) {
            $q->where($i[0], $i[1], $i[2]);
        }
    }
}
