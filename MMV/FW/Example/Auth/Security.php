<?php

namespace MMV\FW\Example\Auth;

use MMV\Auth\Low\Auth\SecurityInterface;
use MMV\FW\Example\Security as BaseSecurity;

class Security implements SecurityInterface
{
    protected BaseSecurity $security;

    public function __construct(BaseSecurity $security)
    {
        $this->security = $security;
    }

    public function hash($value, array $options = []): string
    {
        return $this->security->hash->make($value, $options);
    }

    public function check(string $value, string $hashedValue, array $options = []): bool
    {
        return $this->security->hash->check($value, $hashedValue, $options);
    }

    public function randomString(int $length): string
    {
        return $this->security->randomString($length);
    }
}
