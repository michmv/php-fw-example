<?php

namespace MMV\FW\Example\Auth;

use MMV\FW\Example\Translator;

class Trans
{
    public static function trans(Translator $translator, array $messages)
    {
        foreach($messages as $name => $error) {
            foreach($error as $index => $message) {
                list($message_, $params_) = static::parseMessageValidator($message);
                $error[$index] = $translator->trans('auth@validator.'.$message_, $params_);
            }
            $messages[$name] = $error;
        }
        return $messages;
    }

    protected static function parseMessageValidator($str)
    {
        $res = [];
        $array = explode('|', $str);
        for($n=1; $n<count($array); $n++) {
            $res[':p'.$n] = $array[$n];
        }
        return [$array[0], $res];
    }
}
