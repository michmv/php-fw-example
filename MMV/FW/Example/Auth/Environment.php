<?php

namespace MMV\FW\Example\Auth;

use MMV\Auth\Low\Auth\EnvironmentInterface;
use Symfony\Component\HttpFoundation\Request;

class Environment implements EnvironmentInterface
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getAgentId(): string
    {
        $ips = $this->request->getClientIps();

        return substr(implode('|', $ips), 0, 256);
    }
}
