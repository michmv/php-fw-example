<?php

namespace MMV\FW\Example\Auth;

use MMV\Auth\Low\User as BaseUser;

class User extends BaseUser
{
    public function getRulesInsert(): array
    {
        $res = [];
        foreach($this->access->getRoles() as $role) {
            $res[] = $role->id;
        }

        $rules = parent::getRulesInsert();
        $rules['role'] = 'required|inArray:'.implode(':', $res);
        return $rules;
    }
}
