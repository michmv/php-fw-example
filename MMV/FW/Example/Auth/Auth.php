<?php

namespace MMV\FW\Example\Auth;

use MMV\Auth\Low\Auth as BaseAuth;
use MMV\Auth\Low\Access;
use MMV\FW\Example\Auth\User;

class Auth extends BaseAuth
{
    public $defaulRoleId = 0;

    public function access(): Access
    {
        return $this->access;
    }

    public function makeUser(array $data=[]): User
    {
        $user = new User($this->validator, $this->storage, $this->access);
        $user->tableName = $this->tables['users'];
        $user->role = $this->defaulRoleId;
        foreach($user->listFiled() as $field) {
            if(array_key_exists($field, $data))
                $user->$field = $data[$field];
        }
        return $user;
    }
}