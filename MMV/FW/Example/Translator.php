<?php

namespace MMV\FW\Example;

use Symfony\Component\Translation\Translator as SymfonyTranslator;

class Translator extends SymfonyTranslator
{
    public function trans(?string $id, array $parameters = [], string $domain = null, string $locale = null)
    {
        if(!is_null($id)) {
            $t = explode('@', $id, 2);
            if(count($t) > 1) {
                $key = $t[1];
                $domain = $t[0];
            } else {
                $key = (string)$id;
            }
        }

        $res = parent::trans($key, $parameters, $domain, $locale);

        if($res == $key) return $id;
        else return $res;
    }
}
