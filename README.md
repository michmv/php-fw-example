# My framework :) example build

## Install

```bash
$ composer require mmv/fw-example
```

Copy the contents of the `vendor/mmv/fw-example/copy` to the root of the project.

Add to `composer.json` for section `autoload`:

```json
{
    "autoload": {
        "psr-4": {
            "App\\": "App/"
        }
    }
}
```

Update pathes for composer:


```bash
$ composer dump-autoload
```

Set the php process `write/read` permissions to the storage folder.

Copy file `config/.env-example` as `config/.env`.

Create string for `APP_KEY` in the .env file, for this run the console command in the project root:

```bash
$ php bin/console.php key-generate config/.env
```

Configure the `config/.env` file by specifying the database connection parameters and host parameters.

Run migrations:

```bash
$ php bin/console.php migrate:run
```

Create new user for administration:

```bash
$ php bin/console.php new-user <email> <login>
```

Open `public/index.php` in the browser.
