/**
 * @version 2.0.0
 */

///<reference path="./node_modules/@types/jquery/index.d.ts" />

class PanelAdmin {
    it: JQuery<HTMLElement>
    panelAdminLeft: JQuery<HTMLElement>
    panelAdminRight: JQuery<HTMLElement>
    panelAdminTop: JQuery<HTMLElement>
    leftMenuOffset: number = 15
    topMenuStatus: boolean = false
    leftMenuStatus: boolean = false
    listInitFunctions: Array<((n: PanelAdmin) => void)> = []
    modalWindow: any
    showLoading: boolean = false

    public init(it: JQuery<HTMLElement>) {
        this.it = it
        this.panelAdminLeft = $('#panel-admin-left', this.it)
        this.panelAdminRight = $('#panel-admin-right', this.it)
        this.panelAdminTop = $('#panel-admin-top', this.it)

        this.resize()
        this.it.css('visibility', 'visible')

        // Hide all menu
        this.it.on('click', (() => { this.hideAll() }).bind(this))

        // Left menu. Set event for open menu
        $('.pal-sub > span.pal-text', this.panelAdminLeft).on('click', 
            ((even: any) => { this.subMenu(even.target); return false; }).bind(this))

        // Show top menu
        $('.pat-menu-title', this.panelAdminTop)
            .on('click', (() => { this.topMenu(); return false; }).bind(this))

        // Show left menu
        $('.pat-button', this.panelAdminTop)
            .on('click', (() => { this.leftMenu(); return false; }).bind(this))

        $(this.panelAdminLeft).on('click', ((e: any) => {
            if(this.leftMenuStatus && e.target.nodeName != 'A') return false
        }).bind(this))

        // Left menu formated
        let menu = $('.pal-menu', this.panelAdminLeft)
        this.menuLeftSetOffset(menu, this.leftMenuOffset)
        this.menuLeftOpenSelect(menu)

        // execute custom function
        for(let index in this.listInitFunctions) {
            this.listInitFunctions[index](this)
        }
    }

    public resize() {
        this.hideAll()

        let height = $(window).height()
        let heightTopMenu = this.panelAdminTop.height()
        let heightWorkPlace = height - heightTopMenu

        // Set height top menu
        $('ul', this.panelAdminTop).css('max-height', heightWorkPlace)

        // Set height left menu
        $('#pal-content', this.panelAdminLeft).height(heightWorkPlace)

        // Set height right content
        $(this.panelAdminRight).height(heightWorkPlace)
    }
    
    public addInitFunction(func: ((n: PanelAdmin) => void)) {
        this.listInitFunctions.push(func)
    }
        
    /**
     * @param string type sm | lg | xl
     */
    public openModal(content: JQuery<HTMLElement>, type: string = '', footer?: JQuery<HTMLElement>, header?: JQuery<HTMLElement>, url: string = ''): any {
        // create window
        let con = $('<div>', {class: 'modal-content'})
        let dia = $('<div>', {class: 'modal-dialog'}).append(con)
        if(url) {
            let form = $('<form>', {action: url, method: 'POST', enctype: 'multipart/form-data'})
            con.append(form)
            con = form
        }
        if(type) dia.addClass('modal-'+type)
        let mw: any = $('<div>', {id: 'modalWindow', class: 'modal fade', tabindex: -1}).append(dia)

        if(header)
            con.append($('<div>', {class: 'modal-header'}).append(header))

        con.append($('<div>', {class: 'modal-body'}).append(content))

        if(footer)
            con.append($('<div>', {class: 'modal-footer'}).append(footer))

        mw.modal()
        mw.on('hide.bs.modal', () => {
            $('#modalWindow').remove()
        })
        this.modalWindow = mw
        return mw
    }

    /**
     * @param string type sm | lg | xl
     */
    public simpleModal(title: string, content: JQuery<HTMLElement>, type: string = '', footer?: JQuery<HTMLElement>, url: string = ''): any {
        let header: any = $(document.createDocumentFragment()).append([
            $('<h5 class="modal-title">'+title+'</h5>'),
            $('<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>'),
        ])
        return this.openModal(content, type, footer, header, url)
    }

    public closeModal(): void
    {
        if(this.modalWindow) this.modalWindow.modal('hide')
    }
    
    public loading(on: boolean): void {
        if(on) {
            this.showLoading = true
            setTimeout(function(){
                if(this.showLoading) {
                    let l = $('<div>', {id: 'loading'})
                    l.append('<div class="cssload-container"><div class="cssload-flex-container"><span class="cssload-loading"></span></div></div>')
                    $('body').append(l)
                }
            }.bind(this), 500)
        }
        else {
            this.showLoading = false
            $('#loading').remove()
        }
    }

    public ajax(options: JQueryAjaxSettings): void {
        options.error = function(jqXHR, textStatus, errorThrown) {
            this.loading(false)
            this.closeModal()
            this.simpleModal('Error', $('<div>', {text: jqXHR.status + ' ' + errorThrown}))
        }.bind(this)
        $.ajax(options)
    }

    public clone(obj: any): any {
        let clone = {}
        for (let key in obj) {
            clone[key] = (typeof obj[key] === 'object' && obj[key] !== null) ?
                this.clone(obj[key]) :
                obj[key]
        }
        return clone
    }

    public contentMinHeight(): number {
        let parContent = $('#par-content', this.panelAdminRight)
        let height = this.panelAdminLeft.height()
        return height - (parContent.outerHeight(true) - parContent.height())
    }

    protected subMenu(item: HTMLElement) {
        let li = $(item).parent()
        if(li.hasClass('pal-open')) li.removeClass('pal-open')
        else li.addClass('pal-open')
    }
    
    protected menuLeftOpenSelect(ul: JQuery<HTMLElement>) {
        let select = $('.pal-select', ul)
        if(select.length) {
            select.parents('.pal-sub').addClass('pal-open')
        }
    }
    
    protected menuLeftSetOffset(ul: JQuery<HTMLElement>, offset: number) {
        let li = ul.children('li')
        li.children('.pal-text').each((i, elem) => {
            elem.style.paddingLeft = offset + "px"
            this.menuLeftSetOffset(li.children('ul'), offset + 10)
       })
    }
    
    protected topMenu() {
        if(this.topMenuStatus) {
            this.topMenuHide()
        } else {
            this.topMenuStatus = true
            $('ul', this.panelAdminTop).addClass('pat-show')
            this.leftMenuHide()
        }
    }
    
    protected topMenuHide() {
        this.topMenuStatus = false
        $('ul', this.panelAdminTop).removeClass('pat-show')
    }
    
    protected hideAll() {
        this.topMenuHide()
        this.leftMenuHide()
    }
        
    protected leftMenu() {
        if(this.leftMenuStatus) {
            this.leftMenuHide()
        } else {
            this.leftMenuStatus = true
            this.panelAdminLeft.addClass('pal-show')
            this.topMenuHide()
        }
    }

    protected leftMenuHide() {
        this.leftMenuStatus = false
        this.panelAdminLeft.removeClass('pal-show')
    }
}

let panelAdmin: PanelAdmin = new PanelAdmin()

jQuery(function() {
    panelAdmin.init($('#panel-admin'))
    $(window).on('resize', () => panelAdmin.resize())
})
