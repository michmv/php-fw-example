#!/bin/bash

cd $(dirname $0)

sass main.scss ../../copy/public/assets/panel-admin/main.css

tsc main.ts --outFile ../../copy/public/assets/panel-admin/main.js -sourcemap
